﻿using EJournal.Application.Common.Models;
using EJournal.Application.Notifications.Queries.GetNotification;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace EJournal.Application.Utility
{
    public static class SqlHelpers
    {
        //public static Settings Setting { get; set; }
        public static MerchantInfo GetTerminalDetailByTernimal(string terminalId,string conn)
        {

            if (string.IsNullOrEmpty(conn))
            {
                return new MerchantInfo();
            }
            var sql = $"select top 1 MerchantName,Physical_Address,RecieptFooter FROM tbl_terminalConfig where Terminal_id = '{terminalId}'";
            using System.Data.SqlClient.SqlConnection cnn = new System.Data.SqlClient.SqlConnection(conn);
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand
            {
                Connection = cnn,
                CommandType = CommandType.Text,
                CommandText = sql
            };

            try
            {
                cnn.Open();
                MerchantInfo result = new MerchantInfo();
                using (var reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                {
                    if (reader.Read())
                    {
                        result = new MerchantInfo {MerchantName = reader["MerchantName"].ToString(),PhysicalAddress= reader["Physical_Address"].ToString(),RecieptFooter= reader["RecieptFooter"].ToString() };
                    }
                }
                
                cnn.Close();
               
                return result;
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Error Form Method Feteching data from DB {Environment.NewLine}{Environment.NewLine} ======================>Mesaage<===================" +
                //$"{Environment.NewLine} {ex.Message}{Environment.NewLine} ====================>Internal Error<======================={Environment.NewLine}{ex.InnerException.Message} ")
                return null;
            }
        }

   }
}
