﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EJournal.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Role.Queries
{
    public class GetRoleQuery : IRequest<List<RoleDto>>
    {
        public class GetRoleQueryHandler : IRequestHandler<GetRoleQuery, List<RoleDto>>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public GetRoleQueryHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<RoleDto>> Handle(GetRoleQuery request, CancellationToken cancellationToken)
            {
                var vm = new List<RoleDto>();

                vm = await _context.Roles
                    .ProjectTo<RoleDto>(_mapper.ConfigurationProvider)
                    .OrderBy(t => t.Id)
                    .ToListAsync(cancellationToken);

                return vm;
            }
        }
    }
}
