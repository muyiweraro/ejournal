﻿using EJournal.Application.Common.Mappings;
using System.ComponentModel.DataAnnotations;

namespace EJournal.Application.Role
{
    public class RoleDto : IMapFrom<Domain.Entities.Role>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string Button { get; set; }
    }
}
