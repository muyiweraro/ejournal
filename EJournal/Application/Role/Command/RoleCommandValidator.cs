﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Application.Role
{
    public class RoleCommandValidator: AbstractValidator<RoleCommand>
    {
        public RoleCommandValidator()
        {
            RuleFor(v => v.Name).MaximumLength(100).NotEmpty();
        }
    }
}
