﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Common.Mappings;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Role
{
    public class RoleCommand : IMapFrom<Domain.Entities.Role>, IMapFrom<RoleDto>, IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public class RoleCommandHandler : IRequestHandler<RoleCommand, int>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public RoleCommandHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<int> Handle(RoleCommand request, CancellationToken cancellationToken)
            {

                var entity = _mapper.Map<Domain.Entities.Role>(request);
                if (request.Id > 0)
                {
                    var edit = await _context.Roles.FindAsync(request.Id);
                    edit.Name= entity.Name;
                }
                else
                {
                    await _context.Roles.AddAsync(entity);
                }

                await _context.SaveChangesAsync(cancellationToken);

                return entity.Id;
            }

        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.Role, RoleCommand>().ReverseMap();
            profile.CreateMap<RoleDto, RoleCommand>().ReverseMap();
            
        }
    }


}
