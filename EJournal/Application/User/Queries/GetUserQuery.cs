﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EJournal.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.User.Queries
{
    public class GetUserQuery : IRequest<IEnumerable<UserDto>>
    {
        public class GetUserQueryHandler : IRequestHandler<GetUserQuery, IEnumerable<UserDto>>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public GetUserQueryHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<IEnumerable<UserDto>> Handle(GetUserQuery request, CancellationToken cancellationToken)
            {
                var vm = new List<UserDto>();

                vm = await _context.AppUsers
                    .ProjectTo<UserDto>(_mapper.ConfigurationProvider)
                    .OrderBy(t => t.Id)
                    .ToListAsync(cancellationToken);

                return vm.WithoutPasswords();
            }
        }
    }
}
