﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Utility;
using EJournal.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.User.Queries
{
    public class LoginQuery : IRequest<UserDto>
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public class LoginQueryCommandHandler : IRequestHandler<LoginQuery, UserDto>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public LoginQueryCommandHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<UserDto> Handle(LoginQuery request, CancellationToken cancellationToken)
            {
                try
                {
                    LogHelper.Log("User email: " + request.Username);
                    var hashPassword = Helpers.Encrypt(request.Password);
                    LogHelper.Log("hashPassword: " + hashPassword);
                    var user = await _context.AppUsers
                        .Include(c => c.Client)
                        .Include(r => r.Role)
                        .FirstOrDefaultAsync(u => u.Username == request.Username && u.Password == hashPassword);
                    var users = await _context.AppUsers.ToListAsync();
                    LogHelper.Log("User count on DB: " +  users.Count);
                    LogHelper.Log("Users on DB: " + JsonConvert.SerializeObject(users));
                    LogHelper.Log("User Details: " + JsonConvert.SerializeObject(user));
                    if (user == null)
                    {
                        LogHelper.Log("User record not available on database");
                        return null;
                    }
                    var entity = _mapper.Map<UserDto>(user);
                    entity.Password = string.Empty;

                    return entity;
                }
                catch (Exception ex)
                {
                    LogHelper.Log("Error: " + ex.Message);
                    return null;
                }
               
            }
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AppUser, UserDto>().ReverseMap();
        }
    }
   
}
