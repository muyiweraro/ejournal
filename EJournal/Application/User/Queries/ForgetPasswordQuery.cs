﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Utility;
using EJournal.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.User.Queries
{
    public class ForgetPasswordQuery : IRequest<(bool,string)>
    {
        public string Username { get; set; }
        public string Password { get; set; } = Helpers.GeneratePassword();
        public class ForgetPasswordQueryHandler : IRequestHandler<ForgetPasswordQuery, (bool,string)>
        {
            private readonly ITMSNotificationBridgeContext _context;
           
            public ForgetPasswordQueryHandler(ITMSNotificationBridgeContext context)
            {
                _context = context;
            }
            public async Task<(bool, string)> Handle(ForgetPasswordQuery request, CancellationToken cancellationToken)
            {
                var hashPassword = Helpers.Encrypt(request.Password);
                var user = await _context.AppUsers
                    .FirstOrDefaultAsync(u => u.Username == request.Username);
                if(user == null)
                {
                    return (false, string.Empty);
                }
                user.ResetToken = hashPassword;
                await _context.SaveChangesAsync(cancellationToken);
                return (true, request.Password);
            }
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AppUser, UserDto>().ReverseMap();
        }
    }
   
}
