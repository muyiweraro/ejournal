﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Utility;
using EJournal.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.User.Queries
{
    public class ChangePasswordQuery : IRequest<bool>
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public class ChangePasswordQueryHandler : IRequestHandler<ChangePasswordQuery, bool>
        {
            private readonly ITMSNotificationBridgeContext _context; 
            public ChangePasswordQueryHandler(ITMSNotificationBridgeContext context)
            {
                _context = context;
            }
            public async Task<bool> Handle(ChangePasswordQuery request, CancellationToken cancellationToken)
            {
                bool valid = false;
                var hashPassword = Helpers.Encrypt(request.Password);
                var edit = await _context.AppUsers.FirstOrDefaultAsync(u => u.ResetToken == hashPassword);
                if(edit != null)
                {
                    edit.Password = Helpers.Encrypt(request.NewPassword);
                    edit.ResetToken = string.Empty;
                    await _context.SaveChangesAsync(cancellationToken);
                    valid = true;
                }

                return valid ;
            }
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AppUser, UserDto>().ReverseMap();
        }
    }
   
}
