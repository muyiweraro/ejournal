﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EJournal.Application.User
{
    public static class UserExtention
    {

        public static IEnumerable<UserDto> WithoutPasswords(this IEnumerable<UserDto> users)
        {
            return users.Select(x => x.WithoutPassword());
        }
        public static UserDto WithoutPassword(this UserDto user)
        {
            user.Password = string.Empty;
            return user;
        }
    }
}
