﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Common.Mappings;
using EJournal.Application.Utility;
using EJournal.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.User.Commands
{
    public class UserCommand : IMapFrom<AppUser>, IMapFrom<UserDto>, IRequest<(int,string)>
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; } = Helpers.GeneratePassword();
        public int ClientId { get; set; }
        public int RoleId { get; set; }
        public string Token { get; set; }

        public class UserCommandHandler : IRequestHandler<UserCommand, (int,string)>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public UserCommandHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<(int,string)> Handle(UserCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    int returnValue = 0;
                    string returnKey = string.Empty;
                    LogHelper.Log("User Payload: " + JsonConvert.SerializeObject(request));
                    var token = Helpers.Encrypt(request.Password);
                    var entity = _mapper.Map<AppUser>(request);
                    if (request.Id > 0)
                    {
                        var edit = await _context.AppUsers.FindAsync(request.Id);
                        edit.FirstName = entity.FirstName;
                        edit.LastName = entity.LastName;
                        edit.RoleId = entity.RoleId;
                        returnValue = 0;

                    }
                    else
                    {
                        var userExit = _context.AppUsers.Any(u => u.Username == request.Username);
                        LogHelper.Log("User details if already exist: " + JsonConvert.SerializeObject(userExit));
                        if (userExit)
                        {
                            returnValue = -2;

                            return (returnValue, returnKey);
                        }
                        returnValue = 1;
                        entity.ResetToken = token;
                        entity.Password = string.Empty;
                        await _context.AppUsers.AddAsync(entity);
                        returnValue = entity.Id;
                        returnKey = request.Password;
                    }
                    await _context.SaveChangesAsync(cancellationToken);
                    return (returnValue, returnKey);
                }
                catch (Exception ex)
                {
                    LogHelper.Log("Error: " + ex.Message + " " + ex.StackTrace + ex.Source);
                    throw ex;
                }
               
            }

        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AppUser, UserCommand>().ReverseMap();
            profile.CreateMap<UserDto, UserCommand>().ReverseMap();
            profile.CreateMap<UserDto, AppUser>().ReverseMap()
                .ForMember(des => des.RoleName, m => m.MapFrom(src => src.Role.Name));
           
        }
    }
}
