﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Application.User.Commands
{
    public class UserCommandValidator : AbstractValidator<UserCommand>
    {
        public UserCommandValidator()
        {
            RuleFor(v => v.FirstName)
              .MaximumLength(200)
              .NotEmpty();
            RuleFor(v => v.LastName)
             .MaximumLength(200)
             .NotEmpty();
            RuleFor(v => v.Username)
             .MaximumLength(500)
             .NotEmpty();
            RuleFor(v => v.RoleId)
                .NotNull();
            RuleFor(v => v.ClientId)
               .NotNull();
        }
    }
}
