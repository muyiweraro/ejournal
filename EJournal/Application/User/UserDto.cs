﻿using EJournal.Application.Client;
using EJournal.Application.Common.Mappings;
using EJournal.Application.Role;
using EJournal.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace EJournal.Application.User
{
    public class UserDto : IMapFrom<AppUser>
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Username { get; set; }
        public string Password { get; set; } 
        public string RoleName { get; set; }
        public int ClientId { get; set; }
        public int RoleId { get; set; }
        public virtual RoleDto Role { get; set; }
        public virtual ClientDto Client { get; set; }

    }
}
