﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Application.Common.Models
{
    public class MerchantInfo
    {
        public string MerchantName { get; set; }
        public string PhysicalAddress { get; set; }
        public string RecieptFooter { get; set; }
    }
}
