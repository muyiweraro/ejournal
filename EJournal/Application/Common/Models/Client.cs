﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Application.Common.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientSerect { get; set; }
        public string ClientType { get; set; }
        public string MerchantIds { get; set; }
        public string BankIds { get; set; }
    }
}
