﻿using EJournal.Application.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Application.Common.Models
{
    public class UserClientDetails
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string RoleName { get; set; }
        public int ClientId { get; set; }
        public string token { get; set; }
        public ClientDto Client { get; set; }
    }

}
