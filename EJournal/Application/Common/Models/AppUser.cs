﻿using EJournal.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EJournal.Application.Common.Models
{
    public class AppUser 
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Username { get; set; }
        public string Password { get; set; } = "Pa55word@123";
        public int ClientId { get; set; }
        public string RoleName { get; set; }
       
    }
}
