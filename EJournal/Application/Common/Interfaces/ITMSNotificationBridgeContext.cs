﻿using EJournal.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Common.Interfaces
{
    public interface ITMSNotificationBridgeContext
    {
        public  DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Domain.Entities.Role> Roles { get; set; }
        public DbSet<Billers> Billers { get; set; }
        public DbSet<Domain.Entities.Notification> Notification { get; set; }
        public DbSet<TerminalReqResp> TerminalReqResp { get; set; }
        public DbSet<TransactionLogger> TransactionLogger { get; set; }
        public DbSet<TransactionRow> TransactionRow { get; set; }
        public DbSet<ClientUser> ClientUser { get; set; }
        

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
