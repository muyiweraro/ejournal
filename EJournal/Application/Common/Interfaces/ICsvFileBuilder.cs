﻿using EJournal.Domain.Entities;
using System.Collections.Generic;

namespace EJournal.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<object> records);
    }
}
