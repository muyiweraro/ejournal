﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Common.Interfaces
{
    public interface ICentralCollectionReportContext
    {
        public DbSet<Domain.Entities.Institutions> Institutions { get; set; }
    }
}
