﻿using EJournal.Application.Client;
using EJournal.Application.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EJournal.Application
{
    public interface IIdentityService
    {
        Task<string> GetUserNameAsync(int userId);

        //Task<(Result Result, int UserId)> CreateUserAsync(AppUser appUser);
        //Task<(AppUser user, ClientDto client)> LoginAsync(string username, string password);
        //Task<(AppUser user, Common.Models.Client client)> GetUserInfo(string name);

        Task<Result> DeleteUserAsync(int userId);
        Task<List<string>> GetAllRoleAsync();
    }
}
