﻿using Dapper;
using EJournal.Application.Common.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EJournal.Application.Common.Interfaces
{
    public class DapperRepo : IDapper
    {
        private readonly IConfiguration _config;
        private Settings _settings;
        private string Connectionstring = "DefaultConnection";
        private readonly ILogger<DapperRepo> _logger;

        public DapperRepo(Settings settings, IConfiguration config, ILogger<DapperRepo> logger)
        {
            _config = config;
            _logger = logger;
            _settings = settings;
        }


        public int Execute(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            throw new NotImplementedException();
        }

        public async Task<T> Get<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            using IDbConnection db = new SqlConnection(_config.GetConnectionString(Connectionstring));
            var response = await db.QueryFirstOrDefaultAsync<T>(sp, parms, commandType: commandType);
            return response;
        }

        public List<T> GetAll<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            using IDbConnection db = new SqlConnection(_config.GetConnectionString(Connectionstring));
            
            return db.Query<T>(sp, parms, commandType: commandType, commandTimeout: (int)TimeSpan.FromMinutes(2).TotalSeconds).ToList();
        }

        public DbConnection GetDbconnection()
        {
            return new SqlConnection(_settings.MiniTerminalDbConnection);
        }

        public async Task<int> Insert(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            int result;
            using IDbConnection db = new SqlConnection(_config.GetConnectionString(Connectionstring));
            try
            {
                if (db.State == ConnectionState.Closed)
               db.Open();
            using var tran = db.BeginTransaction();
            try
            {
                result = await db.QueryFirstOrDefaultAsync<int>(sp, parms, commandType: commandType, transaction: tran);
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                _logger.LogError(ex.Message);
                _logger.LogError("Error", ex);
                throw;
            }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError("Error", ex);
                throw;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }
            return result;
        }
    }
}
