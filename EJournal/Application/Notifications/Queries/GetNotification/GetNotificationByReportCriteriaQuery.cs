﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Notification;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Great.EmvTags;
using EJournal.Application.Utility;
using EJournal.Application.Common.Models;
using Microsoft.AspNetCore.Http;
using EJournal.Domain.Enums;
using EJournal.Domain.Entities;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;
using Dapper;

namespace EJournal.Application.Notifications.Queries.GetNotification
{
    public class GetNotificationByReportCriteriaQuery : IRequest<List<Domain.Entities.Notification>>
    {
        public string RRN { get; set; }
        public string STAN { get; set; }
        public string ApprovalCode { get; set; }
        public string MerchantIds { get; set; }
        public string BandkCode { get; set; }
        public string TerminalId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public ClientUser user { get; set; }

        public class GetNotificationByReportCriteriaQueryHandler : IRequestHandler<GetNotificationByReportCriteriaQuery, List<Domain.Entities.Notification>>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;
            public readonly Settings _settings;
            private readonly IDapper _dapper;

            readonly IHttpContextAccessor _httpContextAccessor;
            private readonly IMemoryCache _cache;
            public GetNotificationByReportCriteriaQueryHandler(ITMSNotificationBridgeContext context,IDapper dapper,
                IMapper mapper, Settings settings, IHttpContextAccessor httpContextAccessor, IMemoryCache memoryCache)
            {
                _context = context;
                _mapper = mapper;
                _settings = settings;
                _httpContextAccessor = httpContextAccessor;
                _cache = memoryCache;
                _dapper = dapper;
            }
            public async Task<List<Domain.Entities.Notification>> Handle(GetNotificationByReportCriteriaQuery request, CancellationToken cancellationToken)
            {
               
                try
                {
                    string authHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                    LogHelper.Log("authHeader: " + authHeader);
                    var user = _httpContextAccessor.HttpContext.User;
                    LogHelper.Log("authHeader: " + user);
                    string username = authHeader != null ? Helpers.GetUsername(authHeader) : string.Empty;
                    LogHelper.Log("username: " + username);
                    // _cache.TryGetValue(username, out ClientUser client);
                    ClientUser client = request.user;
                    LogHelper.Log("Client: " + JsonConvert.SerializeObject(client));
                    var IsAdmin = true;
                    if (request.MerchantIds != "All" && client.ClientType == ClientType.Merchant)
                    {
                        IsAdmin = false;
                    }
                    else
                    {
                        request.MerchantIds = "";
                    }
                    DynamicParameters dbPara = new DynamicParameters();
                    dbPara.Add("StartDate", request.StartDate, DbType.String);
                    dbPara.Add("EndDate", request.EndDate, DbType.String);
                    dbPara.Add("MerchantId", request.MerchantIds, DbType.String);
                    dbPara.Add("IsAdmin", IsAdmin, DbType.String);
                    List<Domain.Entities.Notification> list = new List<Domain.Entities.Notification>();
                    try
                    {
                        list = _dapper.GetAll<Domain.Entities.Notification>("[dbo].[sp_GetNotifications]", dbPara, commandType: CommandType.StoredProcedure);
                    }
                    catch (SqlException ex)
                    {
                        LogHelper.Log("SQL Error: " + ex.Message + "   " + ex.StackTrace);
                        return list;
                    }
                    //|| client.ClientType != ClientType.None && string.IsNullOrEmpty(client.BankCode))
                    if (client.ClientType != ClientType.None && string.IsNullOrEmpty(client.MerchantIds))
                    {
                        LogHelper.Log("Client type is Null");
                        List<Domain.Entities.Notification> lists = new List<Domain.Entities.Notification>();
                        return lists;
                    }
                    request.MerchantIds = client.ClientType != ClientType.None ? client.MerchantIds : request.MerchantIds;
                    request.BandkCode = client.ClientType != ClientType.None ? client.BankCode : request.BandkCode;

                    //  var notificationCount = list.Count();
                    // LogHelper.Log("Notification Count: " + notificationCount);


                    //if (!string.IsNullOrEmpty(request.StartDate))
                    //{
                    //    list = list.Where(x => x.Transactiondate.CompareTo(request.StartDate) >= 0).ToList();
                    //    LogHelper.Log("Executed StartDate: ");
                    //}
                    //if (!string.IsNullOrEmpty(request.EndDate))
                    //{
                    //    list = list.Where(x => x.Transactiondate.CompareTo(request.EndDate) <= 0);
                    //    LogHelper.Log("Executed EndDate: ");
                    //}
                    if (!string.IsNullOrEmpty(request.RRN))
                    {
                        var s = list.Where(x => x.Rrn == request.RRN);
                       // LogHelper.Log("Notification Count: " + list.Count());
                    }
                    if (!string.IsNullOrEmpty(request.TerminalId))
                    {
                        list = list.Where(x => x.Terminalid == request.TerminalId).ToList();
                      //  LogHelper.Log("Notification Count: " + list.Count());
                    }
                    if (!string.IsNullOrEmpty(request.STAN))
                    {
                        list = list.Where(x => x.Stan == request.STAN).ToList();
                       // LogHelper.Log("Notification Count: " + list.Count());
                    }
                    
                    //if (list.Any())
                    //{
                    //    if (request.MerchantIds != "All" && client.ClientType == ClientType.Merchant)
                    //    {
                    //        var keywords = request.MerchantIds.Split(',');
                    //        list = list.Where(x => keywords.Contains(x.MerchantId));
                    //        LogHelper.Log("Notification Count: " + list.Count());
                    //    }
                    //    else if (string.IsNullOrEmpty(request.BandkCode) && client.ClientType == ClientType.Bank)
                    //    {
                    //        var keywords = request.BandkCode.Split(',');
                    //        list = list.Where(x => keywords.Contains(x.MerchantId.Substring(1, 3)));
                    //        LogHelper.Log("Notification Count: " + list.Count());
                    //    }
                    //    //else
                    //    //{
                    //    //    var keywords = request.MerchantIds.Split(',');
                    //    //    list = list.Where(x => keywords.Contains(x.MerchantId));
                    //    //}
                    //}
                    //   vm = await list.ProjectTo<NotificationDto>(_mapper.ConfigurationProvider)
                    // .OrderBy(t => t.Id)
                    // .ToListAsync(cancellationToken);
                    //foreach (var item in vm)
                    //{
                    //    string msg = "";
                    //    try
                    //    {
                    //        item.Track2 = !string.IsNullOrEmpty(item.Track2) ? item.Track2.Length > 21 ? item.Track2.Substring(17, 4) : item.Track2 : string.Empty;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        msg = ex.Message;
                    //    }
                    //    try
                    //    {
                    //        item.Amount = !string.IsNullOrEmpty(item.Amount) ? NotifyHelper.GetAmountInNaira(item.Amount) : "0.0";
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        msg = ex.Message;
                    //    }

                    //    try
                    //    {
                    //        if (item.PaxTransaction == true) { item.CustomerName = Helpers.FromHex(item.Track1); };
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        msg = ex.Message;
                    //    }

                    //    if (item.PaxTransaction == null || item.PaxTransaction == false)
                    //    {
                    //        try
                    //        {
                    //            var merchantInfo = SqlHelpers.GetTerminalDetailByTernimal(item.Terminalid, _settings.MiniTerminalDbConnection);
                    //            item.MerchantAddress = !string.IsNullOrEmpty(merchantInfo.PhysicalAddress) ? merchantInfo.PhysicalAddress : item.MerchantAddress;
                    //            item.MerchantName = !string.IsNullOrEmpty(merchantInfo.MerchantName) ? merchantInfo.MerchantName : item.MerchantName;
                    //            item.Footer = !string.IsNullOrEmpty(merchantInfo.RecieptFooter) ? merchantInfo.RecieptFooter : item.Footer;
                    //            LogHelper.Log("PdfItem: " + JsonConvert.SerializeObject(item));
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            msg = ex.Message;
                    //            LogHelper.Log("Error on merchantInfo: " + ex.Message);
                    //        }

                    //    }

                    //}

                    //foreach (var item in vm.Where(item => !string.IsNullOrEmpty(item.Iccdata.Replace(" ", ""))).Select(item => item))
                    //{
                    //    try
                    //    {
                    //        var grt = EmvTagParser.ParseTlvList(item.Iccdata.Trim());
                    //        var getAlternativeCardLabel = !string.IsNullOrEmpty(item.Pan) ? NotifyHelper.GetAlternativeCardLabel(item.Pan) : string.Empty;
                    //        item.Aid = grt.FirstOrDefault(v => v.Tag == "9F06") != null ? grt.FirstOrDefault(v => v.Tag == "9F06").Value.Hex : string.Empty;
                    //        item.Card = grt.FirstOrDefault(v => v.Tag == "50") != null ? grt.FirstOrDefault(v => v.Tag == "50").Value?.Ascii : getAlternativeCardLabel;
                    //        item.Ac = grt.FirstOrDefault(v => v.Tag == "9F26") != null ? grt.FirstOrDefault(v => v.Tag == "9F26").Value?.Hex : string.Empty;
                    //        item.Tvr = grt.FirstOrDefault(v => v.Tag == "95") != null ? grt.FirstOrDefault(v => v.Tag == "95").Value?.Hex : string.Empty;
                    //        item.Tsi = grt.FirstOrDefault(v => v.Tag == "9B") != null ? grt.FirstOrDefault(v => v.Tag == "9B").Value?.Hex : "E800";
                    //        item.Iccdata = string.Empty;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        LogHelper.Log("Error on getAlternativeCardLabel: " + ex.Message);
                    //    }

                    //};
                    // LogHelper.Log("Notification Count: " + list.Count());
                    return list;
                }
                catch (Exception ex)
                {
                    LogHelper.Log("Error on GetNotification: " + ex);
                    List<Domain.Entities.Notification> notifications = new List<Domain.Entities.Notification>();
                    return notifications;
                }

            }


        }
    }
}
