﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Notification;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Great.EmvTags;
using EJournal.Application.Utility;
using EJournal.Application.Common.Models;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
using EJournal.Domain.Entities;
using Microsoft.Extensions.Caching.Memory;


namespace EJournal.Application.Notifications.Queries.GetNotification
{
    public class GetNotificationByThirdPartyQuery : IRequest<List<NotificationDto>>
    {
        public string RRN { get; set; }
        public string STAN { get; set; }
        public string ApprovalCode { get; set; }
        public string TerminalId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        //[NotMapped]
       // public string ClientType { get; set; }

        public class GetNotificationByThirdPartyQueryHandler : IRequestHandler<GetNotificationByThirdPartyQuery, List<NotificationDto>>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;
            public readonly Settings _settings;
            readonly IHttpContextAccessor _httpContextAccessor;
            private readonly IMemoryCache _cache;
            public GetNotificationByThirdPartyQueryHandler(ITMSNotificationBridgeContext context,
                IMapper mapper, Settings settings, IHttpContextAccessor httpContextAccessor, IMemoryCache memoryCache)
            {
                _context = context;
                _mapper = mapper;
                _settings = settings;
                _httpContextAccessor = httpContextAccessor;
                _cache = memoryCache;
            }
            public async Task<List<NotificationDto>> Handle(GetNotificationByThirdPartyQuery request, CancellationToken cancellationToken)
            {
                string authHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                var user = _httpContextAccessor.HttpContext.User;
                string username = authHeader != null ? Helpers.GetUsername(authHeader) : string.Empty;
                _cache.TryGetValue(username, out ClientUser client);
                if (client.ClientType != Domain.Enums.ClientType.None && (string.IsNullOrEmpty(client.BankCode) && string.IsNullOrEmpty(client.MerchantIds)))
                {
                    return new List<NotificationDto>();
                }

                var merchantIds = client.ClientType != Domain.Enums.ClientType.None ? client.MerchantIds : string.Empty;
                var bandkCode = client.ClientType != Domain.Enums.ClientType.None ? client.BankCode : string.Empty;
                var vm = new List<NotificationDto>();
                var list = _context.Notification.AsQueryable();
                if (!string.IsNullOrEmpty(request.StartDate))
                {
                    list = list.Where(x => x.Transactiondate.CompareTo(request.StartDate) >= 0);
                }
                if (!string.IsNullOrEmpty(request.EndDate))
                {
                    list = list.Where(x => x.Transactiondate.CompareTo(request.EndDate) <= 0);

                }
                if (!string.IsNullOrEmpty(request.RRN))
                {
                    list = list.Where(x => x.Rrn == request.RRN);
                }
                if (!string.IsNullOrEmpty(request.TerminalId))
                {
                    list = list.Where(x => x.Terminalid == request.TerminalId);
                }
                if (!string.IsNullOrEmpty(request.STAN))
                {
                    list = list.Where(x => x.Stan == request.STAN);
                }
                if (list.Any())
                {
                    if (merchantIds != "All" && client.ClientType == Domain.Enums.ClientType.Merchant)
                    {
                        var keywords = merchantIds.Split(',');
                        list = list.Where(x => keywords.Contains(x.MerchantId));
                    }
                    else if (!string.IsNullOrEmpty(bandkCode) && client.ClientType == Domain.Enums.ClientType.Bank)
                    {
                        var keywords = bandkCode.Split(',');
                        list = list.Where(x => keywords.Contains(x.MerchantId.Substring(1, 3)));
                    }else
                    {
                        return new List<NotificationDto>();
                    }
                }else
                {
                    return new List<NotificationDto>();
                }
                vm = await list.ProjectTo<NotificationDto>(_mapper.ConfigurationProvider)
                .OrderBy(t => t.Id)
                .ToListAsync(cancellationToken);
                foreach (var item in vm)
                {
                    item.Track2 = !string.IsNullOrEmpty(item.Track2) ? item.Track2.Substring(17, 4) : string.Empty;
                    item.Amount = !string.IsNullOrEmpty(item.Amount) ? NotifyHelper.GetAmountInNaira(item.Amount) : "0.0";
                    if (item.PaxTransaction == true) { item.CustomerName = Helpers.FromHex(item.Track1); };
                    if (item.PaxTransaction == null || item.PaxTransaction == false)
                    {
                        var merchantInfo = SqlHelpers.GetTerminalDetailByTernimal(item.Terminalid, _settings.MiniTerminalDbConnection);
                        item.MerchantAddress = !string.IsNullOrEmpty(merchantInfo.PhysicalAddress) ? merchantInfo.PhysicalAddress : item.MerchantAddress;
                        item.MerchantName = !string.IsNullOrEmpty(merchantInfo.MerchantName) ? merchantInfo.MerchantName : item.MerchantName;
                        item.Footer = !string.IsNullOrEmpty(merchantInfo.RecieptFooter) ? merchantInfo.RecieptFooter : item.Footer;
                    }

                }
                foreach (var item in vm.Where(item => !string.IsNullOrEmpty(item.Iccdata)).Select(item => item))
                {
                    var grt = EmvTagParser.ParseTlvList(item.Iccdata.Trim());
                    var getAlternativeCardLabel = !string.IsNullOrEmpty(item.Pan) ? NotifyHelper.GetAlternativeCardLabel(item.Pan) : string.Empty;
                    item.Aid = grt.FirstOrDefault(v => v.Tag == "9F06") != null ? grt.FirstOrDefault(v => v.Tag == "9F06").Value.Hex : string.Empty;
                    item.Card = grt.FirstOrDefault(v => v.Tag == "50") != null ? grt.FirstOrDefault(v => v.Tag == "50").Value?.Ascii : getAlternativeCardLabel;
                    item.Ac = grt.FirstOrDefault(v => v.Tag == "9F26") != null ? grt.FirstOrDefault(v => v.Tag == "9F26").Value?.Hex : string.Empty;
                    item.Tvr = grt.FirstOrDefault(v => v.Tag == "95") != null ? grt.FirstOrDefault(v => v.Tag == "95").Value?.Hex : string.Empty;
                    item.Tsi = grt.FirstOrDefault(v => v.Tag == "9B") != null ? grt.FirstOrDefault(v => v.Tag == "9B").Value?.Hex : "E800";
                    item.Iccdata = string.Empty;
                };
                return vm;
            }
        }
    }
}
