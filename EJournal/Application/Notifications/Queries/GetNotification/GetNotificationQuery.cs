﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Utility;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Notification.Queries.GetNotification
{
    public class GetUserQuery : IRequest<List<NotificationDto>>
    {
        public class GetNotificationQueryHandler : IRequestHandler<GetUserQuery, List<NotificationDto>>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public GetNotificationQueryHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<NotificationDto>> Handle(GetUserQuery request, CancellationToken cancellationToken)
            {
                var vm = new List<NotificationDto>();
                try
                {
                    vm = await _context.Notification
                  .ProjectTo<NotificationDto>(_mapper.ConfigurationProvider)
                  .OrderBy(t => t.Id)
                  .ToListAsync(cancellationToken);
                    LogHelper.Log("Number of notification details: " + vm.Count);
                    return vm;
                }
                catch (Exception ex)
                {
                    LogHelper.Log("Error: " + ex.Message);
                    return vm;
                }
              
            }
        }
    }
}
