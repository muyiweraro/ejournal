﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Application.Notifications.Queries.GetNotification
{
    public static class NotifyHelper
    {
        public static string GetAmountInNaira(string amount)
        {
            return (Convert.ToDecimal(amount) / 100).ToString("N2");
        }

        public static string GetAlternativeCardLabel(string pan)
        {
            var card = string.Empty;
            if (pan.StartsWith("4"))
            {
                card = "Visa";
            }
            else if (pan.StartsWith("5") && !pan.StartsWith("506"))
            {
                card = "Master Card";
            }
            else if (pan.StartsWith("506") && !pan.StartsWith("5"))
            {
                card = "Master Card";
            }
            return card;
        }
    }
}
