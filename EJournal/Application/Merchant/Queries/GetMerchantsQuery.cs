﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Utility;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Merchant.Queries
{
    public class GetMerchantsQuery : IRequest<List<MerchantDTO>> 
    {
        public class GetMerchantsQueryHandler : IRequestHandler<GetMerchantsQuery, List<MerchantDTO>> 
        {
            private readonly ICentralCollectionReportContext _context;
            private readonly IMapper _mapper;

            public GetMerchantsQueryHandler(ICentralCollectionReportContext context, IMapper mapper) 

            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<MerchantDTO>> Handle(GetMerchantsQuery request,CancellationToken cancellationToken)
            {
                try
                {
                    var vm = new List<MerchantDTO>();

                    vm = await _context.Institutions
                        .ProjectTo<MerchantDTO>(_mapper.ConfigurationProvider)
                        .OrderBy(t => t.id)
                        .ToListAsync(cancellationToken);
                    LogHelper.Log("Merchant: " + JsonConvert.SerializeObject(vm));
                    return vm;
                }
                catch (Exception ex)
                {
                    LogHelper.Log("Error: " + JsonConvert.SerializeObject(ex));
                    var vm = new List<MerchantDTO>();
                    return vm;
                }
               
            }

            //public Task<List<MerchantDTO>> Handle(GetMerchantsQuery request, CancellationToken cancellationToken)
            //{
            //    throw new NotImplementedException();
            //}
        }
       
    }
}
