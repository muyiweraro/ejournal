﻿using EJournal.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Application.Merchant
{
    public class MerchantDTO : IMapFrom<Domain.Entities.Institutions>
    {
        public int id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string StateCode { get; set; }
        public string MinistryCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Address { get; set; }
        public int? InstitutionTypeId { get; set; }
        public int? StateId { get; set; }
        public int? MinistryId { get; set; }
        public string Image { get; set; }
        public Nullable<bool> Active { get; set; }

        public Nullable<bool> IsRevenue { get; set; }
        public Nullable<bool> IsValidate { get; set; }
        public string ValidateUrl { get; set; }
        public Nullable<bool> IsNotify { get; set; }
        public string NotifyUrl { get; set; }
        public string MethodName { get; set; }
        public string BillerCode { get; set; }
        public Nullable<bool> IsVASEndPoint { get; set; }
        public Nullable<bool> IsInstantSettlement { get; set; }
        public string AgentCode { get; set; }
        public string WalletId { get; set; }
        public string AccountNumber { get; set; }
        public string AgentCategory { get; set; }
        public string NarrationFormat { get; set; }
    }
}
