﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Client.Queries
{
    public class GetClientByIdQuery : IRequest<ClientDto>
    {
        public int Id { get; set; }
        public class GetClientByIdQueryCommandHandler : IRequestHandler<GetClientByIdQuery, ClientDto>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public GetClientByIdQueryCommandHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<ClientDto> Handle(GetClientByIdQuery request, CancellationToken cancellationToken)
            {
                var client = await _context.ClientUser.FindAsync(request.Id);
                var entity = _mapper.Map<ClientDto>(client);

                return entity;
            }
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ClientUser, ClientDto>().ReverseMap();
        }
    }
}
