﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Utility;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Client.Queries
{
    public class GetClientQuery : IRequest<List<ClientDto>>
    {
        public class GetClientQueryHandler : IRequestHandler<GetClientQuery, List<ClientDto>>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public GetClientQueryHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<ClientDto>> Handle(GetClientQuery request, CancellationToken cancellationToken)
            {
                var vm = new List<ClientDto>();

                
                vm = await _context.ClientUser.AsNoTracking()
                    .ProjectTo<ClientDto>(_mapper.ConfigurationProvider)
                    .OrderBy(t => t.Id)
                    .ToListAsync(cancellationToken);
                LogHelper.Log("ClientUser: " +  JsonConvert.SerializeObject(vm));
                return vm;
            }
        }
    }
}
