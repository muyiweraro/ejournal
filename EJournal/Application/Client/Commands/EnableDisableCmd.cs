﻿using EJournal.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Client.Commands
{
    public class EnableDisableCmd: IRequest<bool>
    {
        public int ClientId { get; set; }
        public bool Status { get; set; }


        public class EnableDisableCmdHandler : IRequestHandler<EnableDisableCmd, bool>
        {
            private readonly ITMSNotificationBridgeContext _context;
            public EnableDisableCmdHandler(ITMSNotificationBridgeContext context)
            {
                _context = context;
            }
            public async Task<bool> Handle(EnableDisableCmd request, CancellationToken cancellationToken)
            {
                var rId = await _context.ClientUser.FindAsync(request.ClientId);
                rId.Active = request.Status;
           var status =    await _context.SaveChangesAsync(cancellationToken);

                return true;
            }
        }
    }
}
