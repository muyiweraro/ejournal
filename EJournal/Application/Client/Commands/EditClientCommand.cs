﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Common.Mappings;
using EJournal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Client.Commands
{
    public class EditClientCommand : IMapFrom<ClientUser>, IMapFrom<ClientDto>, IRequest<int>
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string BankCode { get; set; }
        public string MerchantIds { get; set; }
        public object ClientType { get; set; }
        public string UserId { get; set; }
       // public string UserSecret { get; set; }
        public string UrlPath { get; set; }
        public bool Active { get; set; }
        //public bool IsDelete { get; set; }


        public class EditClientCommandHandler : IRequestHandler<EditClientCommand, int>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public EditClientCommandHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<int> Handle(EditClientCommand request, CancellationToken cancellationToken)
            {
                 
                var entity = _mapper.Map<ClientUser>(request);
                var edit = await _context.ClientUser.FindAsync(request.Id);
                edit.UserId = entity.UserId;
                edit.Active = entity.Active;
                edit.BankCode = entity.BankCode;
                edit.MerchantIds = entity.MerchantIds;
                edit.ClientType = entity.ClientType;
                edit.ClientName = entity.ClientName;
                edit.ClientEmail = entity.ClientEmail;
                edit.UrlPath = entity.UrlPath;
                await _context.SaveChangesAsync(cancellationToken);

              return  entity.Id;
            }

        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ClientUser, EditClientCommand>().ReverseMap();
            profile.CreateMap<ClientDto, EditClientCommand>().ReverseMap();
            //.ForMember(d => d.Priority, opt => opt.MapFrom(s => (int)s.Priority));
        }
        
    }

   
}
