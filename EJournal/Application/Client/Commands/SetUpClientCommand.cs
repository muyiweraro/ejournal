﻿using AutoMapper;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Common.Mappings;
using EJournal.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Application.Client.Commands
{
    public class SetUpClientCommand : IMapFrom<ClientUser>, IMapFrom<ClientDto>, IRequest<(int,string)>
    {
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string BankCode { get; set; }
        public string MerchantIds { get; set; }
        public object ClientType { get; set; }
        public string UserId { get; set; }
       // public string UserSecret { get; set; }
        public string UrlPath { get; set; }
        public bool Active { get; set; }
        //public bool IsDelete { get; set; }


        public class SetUpClientCommandHandler : IRequestHandler<SetUpClientCommand, (int, string)>
        {
            private readonly ITMSNotificationBridgeContext _context;
            private readonly IMapper _mapper;

            public SetUpClientCommandHandler(ITMSNotificationBridgeContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<(int,string)> Handle(SetUpClientCommand request, CancellationToken cancellationToken)
            {
                var exist = await _context.ClientUser.AnyAsync(c => c.ClientEmail == request.ClientEmail && c.UserId == request.UserId);
                if (exist)
                {
                    return (-2, string.Empty);
                }
                var entity = _mapper.Map<ClientUser>(request);
                entity.UserSecret = Guid.NewGuid().ToString();
                
                _context.ClientUser.Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

              return  (entity.Id, entity.UserSecret);
            }

        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ClientUser, SetUpClientCommand>().ReverseMap();
            profile.CreateMap<ClientDto, SetUpClientCommand>().ReverseMap();
            //.ForMember(d => d.Priority, opt => opt.MapFrom(s => (int)s.Priority));
        }
        
    }

   
}
