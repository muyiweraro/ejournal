﻿using AutoMapper;
using EJournal.Application.Client.Commands;
using EJournal.Application.Common.Mappings;
using EJournal.Domain.Entities;
using EJournal.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EJournal.Application.Client
{
    public class ClientDto: IValidatableObject, IMapFrom<ClientUser>
    {
        public int Id { get; set; }
        [Required]
        public string ClientName { get; set; }
        public string BankCode { get; set; }
        public string MerchantIds { get; set; }
        [EnumDataType(typeof(ClientType))]
        public ClientType ClientType { get; set; } 
        [Required(ErrorMessage = "Client ID is required")]
        public string UserId { get; set; }
        public string UserSecret { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string ClientEmail { get; set; }
        [Required]
        [Range(typeof(bool), "true", "true",
        ErrorMessage = "This form disallows unapproved client.")]
        public bool Active { get; set; }
        public string UrlPath { get; set; }
        // public bool IsDelete { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap<ClientUser, ClientDto>().ReverseMap();
            //.ForMember(d => d.Priority, opt => opt.MapFrom(s => (int)s.Priority));
        }

        public IEnumerable<ValidationResult> Validate(System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            if (ClientType == ClientType.Merchant && string.IsNullOrEmpty(MerchantIds))
            {
                yield return new ValidationResult(
                    $"Merchant Ids is required.",
                    new[] { nameof(MerchantIds) });
            }
            if (ClientType == ClientType.Bank && string.IsNullOrEmpty(BankCode))
            {
                yield return new ValidationResult(
                    $"Bank Code is required.",
                    new[] { nameof(BankCode) });
            }
        }
    }
    
}
