﻿using Blazor.FlexGrid.Components.Configuration;
using Blazor.FlexGrid.Components.Configuration.Builders;
using EJournal.Application.Role;

namespace WebApi.GridConfig
{
    public class RoleGridConfig : IEntityTypeConfiguration<RoleDto>
    {
        public void Configure(EntityTypeBuilder<RoleDto> builder)
        {
            builder.Property(e => e.Id)
            .IsVisible(false);

            builder.Property(e => e.Button)
          .HasCaption(" ");

            



        }
    }
}
