﻿using Blazor.FlexGrid.Components.Configuration;
using Blazor.FlexGrid.Components.Configuration.Builders;
using EJournal.Application.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.GridConfig
{
    public class NotificationGridConfig : IEntityTypeConfiguration<NotificationDto>
    {
        public void Configure(EntityTypeBuilder<NotificationDto> builder)
        {

            builder.Property(e => e.Terminalid)
                .HasCaption("Terminal ID");
            builder.Property(e => e.CustomerName)
               .HasCaption("Customer");
            builder.Property(e => e.MerchantId)
              .HasCaption("Merchant ID")
              .IsFilterable();
            builder.Property(e => e.Transactiondate)
           .HasCaption("Date")
           .IsSortable();

            builder.Property(e => e.Accounttype)
              .IsVisible(false);
            builder.Property(e => e.Aid)
             .IsVisible(false);
            builder.Property(e => e.Aurhorisationresponse)
             .IsVisible(false);
            builder.Property(e => e.Card)
              .IsVisible(false);
            builder.Property(e => e.Cellinfo)
             .IsVisible(false);
            builder.Property(e => e.ChannelCode)
             .IsVisible(false);

            builder.Property(e => e.CreatedDate)
             .IsVisible(false);
            builder.Property(e => e.Currencycode)
             .IsVisible(false);
            builder.Property(e => e.CustomerOtherInfo)
             .IsVisible(false);

            builder.Property(e => e.Datelocaltransaction)
             .IsVisible(false);
            builder.Property(e => e.FirmwareId)
             .IsVisible(false);
            builder.Property(e => e.Footer)
             .IsVisible(false); builder.Property(e => e.Ac)
             .IsVisible(false);
            builder.Property(e => e.Gpsinfo)
             .IsVisible(false);
            builder.Property(e => e.Iccdata)
             .IsVisible(false);builder.Property(e => e.Id)
             .IsVisible(false);
            builder.Property(e => e.MerchantAddress)
             .IsVisible(false);
            builder.Property(e => e.Narration)
             .IsVisible(false);

            builder.Property(e => e.Mti)
             .IsVisible(false);
            builder.Property(e => e.Otherterminalid)
             .IsVisible(false);
            builder.Property(e => e.PaxTransaction)
             .IsVisible(false); builder.Property(e => e.PaymentMethod)
             .IsVisible(false);
            builder.Property(e => e.Posconditioncode)
             .IsVisible(false);
            builder.Property(e => e.Posentrymode)
             .IsVisible(false); builder.Property(e => e.Processingcode)
              .IsVisible(false);
            builder.Property(e => e.Refcode)
             .IsVisible(false);
            builder.Property(e => e.RemoteServerIp)
             .IsVisible(false);


            builder.Property(e => e.ReportPushed)
             .IsVisible(false);
            builder.Property(e => e.ReportPushedDate)
             .IsVisible(false);
            builder.Property(e => e.Responcecode)
             .IsVisible(false); builder.Property(e => e.Responsedescription)
             .IsVisible(false);
            builder.Property(e => e.RevenueCode)
             .HasCaption(" ");
            builder.Property(e => e.Searchtext)
             .IsVisible(false); builder.Property(e => e.Systemtime)
              .IsVisible(false);
            builder.Property(e => e.TmsPushed)
             .IsVisible(false);
            builder.Property(e => e.TmsPushedDate)
             .IsVisible(false);

            builder.Property(e => e.Track1)
             .IsVisible(false);
            builder.Property(e => e.Track2)
             .IsVisible(false);
            builder.Property(e => e.Trancode)
             .IsVisible(false); builder.Property(e => e.Tvr)
             .IsVisible(false);
            builder.Property(e => e.TStatus)
             .IsVisible(false);
            builder.Property(e => e.Tsi)
             .IsVisible(false); builder.Property(e => e.Trantype)
              .IsVisible(false);
            builder.Property(e => e.Transactiontime)
             .IsVisible(false);
            builder.Property(e => e.MerchantName)
             .IsVisible(false);
            builder.Property(e => e.Timelocaltransaction)
             .IsVisible(false);
            builder.Property(e => e.Batchno)
             .IsVisible(false);
            builder.Property(e => e.Seqno)
             .IsVisible(false);

        }
    }
}
