﻿using Blazor.FlexGrid.Components.Configuration;
using Blazor.FlexGrid.Components.Configuration.Builders;
using EJournal.Application.Notification;
using EJournal.Application.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.GridConfig
{
    public class UserGridConfig : IEntityTypeConfiguration<UserDto>
    {
        public void Configure(EntityTypeBuilder<UserDto> builder)
        {
            builder.Property(e => e.Role)
              .IsVisible(false);
            builder.Property(e => e.Id)
           .IsVisible(false);
            builder.Property(e => e.RoleId)
             .HasCaption(" ");
            builder.Property(e => e.RoleName)
            .HasCaption("Role");
            //builder.Property(e => e.Token)
            // .IsVisible(false);
            builder.Property(e => e.Password)
              .IsVisible(false);
            builder.Property(e => e.ClientId)
             .IsVisible(false);
            builder.Property(e => e.Client)
             .IsVisible(false);

        }
    }
}
