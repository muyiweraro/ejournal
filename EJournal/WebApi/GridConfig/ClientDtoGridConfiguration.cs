﻿using Blazor.FlexGrid.Components.Configuration;
using Blazor.FlexGrid.Components.Configuration.Builders;
using EJournal.Application.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.GridConfig
{
    public class ClientDtoGridConfiguration : IEntityTypeConfiguration<ClientDto>
    {
        public void Configure(EntityTypeBuilder<ClientDto> builder)
        {
            builder.Property(e => e.BankCode)
             .HasCaption("Bank Code");
            //.HasValueFormatter(d => d.ToShortDateString());

            builder.Property(e => e.ClientEmail)
                .HasCaption("Email");
            builder.Property(e => e.ClientName)
               .HasCaption("Client Name")
               .IsSortable()
               .IsFilterable();
            builder.Property(e => e.MerchantIds)
              .HasCaption("Merchant ID")
              .IsSortable();
               builder.Property(e => e.UserId)
              .HasCaption("User ID")
              .IsSortable();
            builder.Property(e => e.ClientType)
              .IsVisible(false);
            //builder.Property(e => e.UrlPath)
            // .IsVisible(false);
            builder.Property(e => e.UserSecret)
             .IsVisible(false);
            builder.Property(e => e.UrlPath)
            .HasCaption(" ");
            //.HasOrder(1)
            //.HasValueFormatter(s => $"{s}!");

        }
    }
}
