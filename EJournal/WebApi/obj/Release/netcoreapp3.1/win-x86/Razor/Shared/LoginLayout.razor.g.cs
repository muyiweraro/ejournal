#pragma checksum "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\Shared\LoginLayout.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0593bd1e7607a5e5a44c6ea1f65546fa19eb1d90"
// <auto-generated/>
#pragma warning disable 1591
namespace WebApi.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using WebApi;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using WebApi.Dto;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using WebApi.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using EJournal.Application.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using EJournal.Application.Notification;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Blazored.Toast;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Blazored.Toast.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using CurrieTechnologies.Razor.SweetAlert2;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using System.Net.Http.Headers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using System.Text;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using WebApi.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using WebApi.HttpServices;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using WebApi.Pages.Account;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Blazored;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
    public partial class LoginLayout : LayoutComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "col-12");
            __builder.AddMarkupContent(2, "\r\n    <br><br>\r\n    ");
            __builder.AddContent(3, 
#nullable restore
#line 5 "C:\Users\olumuyiwa.aro\Documents\Visual Studio 2017\Projects\E-Journal\EJournal\WebApi\Shared\LoginLayout.razor"
     Body

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(4, "\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
