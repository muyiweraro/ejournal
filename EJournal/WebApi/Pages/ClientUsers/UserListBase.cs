﻿using Blazor.FlexGrid.DataAdapters;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazored.Toast.Services;
using CurrieTechnologies.Razor.SweetAlert2;
using EJournal.Application.Client;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebApi.Components;
using WebApi.HttpServices;

namespace WebApi.Pages.ClientUsers
{
    public class UserListBase:ComponentBase
    {
        [Inject]
        private HttpClient Http { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        [Inject]
        protected IModalService Modal { get; set; }
        [Inject]
        public IToastService ToastService { get; set; }
        [Inject]
        public SweetAlertService Swal { get; set; }
        [Inject]
        protected AppState AppState { get; set; }
        public CollectionTableDataAdapter<ClientDto> DataAdapter { get; set; }
        public IEnumerable<ClientDto> userList;
        public ClientDto clientObject = new ClientDto();
        public string customHeader = "Set-Up Client User";


        protected override async Task OnInitializedAsync()
        {
      
            if (AppState.IsLoggedIn)
            {
                var token =await AppState.GetUserTokenSession();
                Http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                await LoadData();
              
            }
            else
            {
               // userList = new List<ClientDto>();
            }
          
        }

        private async Task  LoadData()
        {
            
            var list = (await Http.GetJsonAsync<ClientDto[]>($"/api/ClientUsers")).ToList();
            userList = list;
            DataAdapter = new CollectionTableDataAdapter<ClientDto>(list);
        }

        public async Task ClientSetUp()
        {

            var parameters = new ModalParameters();
            parameters.Add("ClientInfo", "");
            var formModal = Modal.Show<ClientDetails>("Client Set-Up Form", parameters);
            var result = await formModal.Result;

            if (result.Cancelled)
            {
                Console.WriteLine("Modal was cancelled");
            }
            else
            {
                ToastService.ShowSuccess(result.Data.ToString(), "Success!");
                await DataChanged();
            }
        }
        public async Task DeleteSymbol(int id)
        {
            bool confirmed = await jsRuntime.InvokeAsync<bool>("confirm", "Are you sure?");
            if (confirmed)
            {
                // Delete!
                ToastService.ShowSuccess("Client deleted successfully", "success!");
            }
        }
        public async Task EditClient(int id)
        {
            var rId = userList.FirstOrDefault(x => x.Id == id);
            if (rId == null)
            {
                // Delete!
                ToastService.ShowError("Client not found", "Error!");
            }
            else
            {
                clientObject = rId;
                var parameters = new ModalParameters();
                var json = JsonConvert.SerializeObject(clientObject);
                parameters.Add("ClientInfo", json);
                
           var formModal =   Modal.Show<ClientDetails>("Edit Client", parameters);
                var result = await formModal.Result;

                if (result.Cancelled)
                {
                    Console.WriteLine("Modal was cancelled");
                }
                else
                {
                    ToastService.ShowSuccess(result.Data.ToString(), "Success!");
                  await  DataChanged();
                }

            }
            //var ter = BlazoredModal;

        }


        public async Task DisableEnable(int id,bool status)
        {
            var passObj = new
            {
                ClientId = id,
                Status = status
            };
            string title = status ? "Enable" : "Disable";
           
            // async/await
            try
            {
                SweetAlertResult result = await Swal.FireAsync(new SweetAlertOptions
                {
                    Title = $"'<small>Are you sure you want to {title}?</small>'",
                    Text = "You will not be able to recover this change!",
                    Icon = SweetAlertIcon.Warning,
                    ShowCancelButton = true,
                    ConfirmButtonText = $"Yes",
                    CancelButtonText = "Not"
                }).ConfigureAwait(false);

                if (!string.IsNullOrEmpty(result.Value))
                {
                    var result1 = await Http.PostJsonAsync<bool>("/api/ClientUsers/DisableClient", passObj);
                    if (result1)
                    {
                        ToastService.ShowSuccess("Request treated successfully", "Client!");
                        await LoadData();
                        await DataAdapter.ReloadCurrentPage();
                    }
                    else
                    {
                        ToastService.ShowError("Could not treat your request at this time.", "Client!");
                    }

                }
            }
            catch (OperationCanceledException ce)
            {
                // CancellationToken.None();
            }
            catch (Exception ex)
            {

            }
           
        }
        public async Task DataChanged()
        {
            await LoadData();
            //StateHasChanged();
            await DataAdapter.ReloadCurrentPage();
        }
       
    }
}
