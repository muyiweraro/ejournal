﻿using Blazor.FlexGrid.DataAdapters;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazored.Toast.Services;
using CurrieTechnologies.Razor.SweetAlert2;
using EJournal.Application.Role;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApi.HttpServices;
using WebApi.Components;

namespace WebApi.Pages.Admin
{
    public class RoleManagerBase : ComponentBase
    {
        [Inject]
        public IUserManagerService UserManagerService { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        [Inject]
        protected IModalService Modal { get; set; }
        [Inject]
        public IToastService ToastService { get; set; }
        [Inject]
        public SweetAlertService Swal { get; set; }
        [Inject]
        protected AppState AppState { get; set; }
        public CollectionTableDataAdapter<RoleDto> DataAdapter { get; set; }
        public IEnumerable<RoleDto> roleList;
        public RoleDto roleObject = new RoleDto();


        protected override async Task OnInitializedAsync()
        {

            if (AppState.IsLoggedIn)
            {

                await LoadData();

            }
            else
            {
                // userList = new List<ClientDto>();
            }

        }

        private async Task LoadData()
        {
            var list = await UserManagerService.GetRoleAsync();
            roleList = list;
            DataAdapter = new CollectionTableDataAdapter<RoleDto>(list);
        }

        public async Task RoleSetUp()
        {
            var formModal = Modal.Show<EditRole>("Role Set-Up Form");
            var result = await formModal.Result;

            if (result.Cancelled)
            {
                Console.WriteLine("Modal was cancelled");
            }
            else
            {
                ToastService.ShowSuccess(result.Data.ToString(), "Success!");
                await DataChanged();
            }
        }
        public async Task DeleteSymbol(int id)
        {
            bool confirmed = await jsRuntime.InvokeAsync<bool>("confirm", "Are you sure?");
            if (confirmed)
            {
                // Delete!
                ToastService.ShowSuccess("User deleted successfully", "success!");
            }
        }
        public async Task ModifyRole(int id)
        {
            var rId = roleList.FirstOrDefault(x => x.Id == id);
            if (rId == null)
            {
                // Delete!
                ToastService.ShowError("User not found", "Error!");
            }
            else
            {
                roleObject = rId;
                var parameters = new ModalParameters();
                var json = JsonConvert.SerializeObject(roleObject);
                parameters.Add("RoleInfo", json);

                var formModal = Modal.Show<EditRole>("Edit Role", parameters);
                var result = await formModal.Result;

                if (result.Cancelled)
                {
                    Console.WriteLine("Modal was cancelled");
                }
                else
                {
                    ToastService.ShowSuccess(result.Data.ToString(), "Success!");
                    await DataChanged();
                }

            }
            //var ter = BlazoredModal;

        }
        public async Task DisableEnable(int id, bool status)
        {
            var passObj = new
            {
                ClientId = id,
                Status = status
            };
            string title = status ? "Enable" : "Disable";

            // async/await
            try
            {
                SweetAlertResult result = await Swal.FireAsync(new SweetAlertOptions
                {
                    Title = $"'<small>Are you sure you want to {title}?</small>'",
                    Text = "You will not be able to recover this change!",
                    Icon = SweetAlertIcon.Warning,
                    ShowCancelButton = true,
                    ConfirmButtonText = $"Yes",
                    CancelButtonText = "Not"
                }).ConfigureAwait(false);

                if (!string.IsNullOrEmpty(result.Value))
                {

                    //var result1 = await Http.PostJsonAsync<bool>("/api/ClientUsers/DisableClient", passObj);
                    if (true)
                    {
                        ToastService.ShowSuccess("Request treated successfully", "Client!");
                        await LoadData();
                        await DataAdapter.ReloadCurrentPage();
                    }
                    else
                    {
                        ToastService.ShowError("Could not treat your request at this time.", "Client!");
                    }

                }
            }
            catch (OperationCanceledException ce)
            {
                // CancellationToken.None();
            }
            catch (Exception ex)
            {

            }

        }
        public async Task DataChanged()
        {
            await LoadData();
            //StateHasChanged();
            await DataAdapter.ReloadCurrentPage();
        }

    }
}
