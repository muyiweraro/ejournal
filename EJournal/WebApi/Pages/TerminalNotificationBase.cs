﻿using Blazor.FlexGrid.DataAdapters;
using BlazorDateRangePicker;
using Blazored.LocalStorage;
using EJournal.Application.Common.Models;
using EJournal.Application.Notification;
using EJournal.Domain.Enums;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using WebApi.Dto;
using WebApi.Components;
using WebApi.Helpers;
using Newtonsoft.Json;
using EJournal.Domain.Entities;
using Microsoft.JSInterop;

namespace WebApi.Pages
{
    public class TerminalNotificationBase : ComponentBase
    {
        protected TerminalSearchModel TerminalSearch { get; set; } = new TerminalSearchModel();
        [Inject]
        private HttpClient Http { get; set; }
        [Inject]
        private ILocalStorageService LocalStorageService { get; set; }
        public CollectionTableDataAdapter<Notification> Notification { get; set; }

        protected TerminalNotificationDialogBase TerminalNotificationDialog { get; set; }
        public List<Notification> notifyList { get; set; }
        public Notification NotificationDto { get; set; } = new Notification();
        public bool Loading { get; set; } = false;
        public int Count { get; set; }
        protected void OnRangeSelect(DateRange range)
        {
            TerminalSearch.StartDate = range.Start.ToString("yyyy-MM-dd");
            TerminalSearch.EndDate = range.End.ToString("yyyy-MM-dd");
            //Use range.Start and range.End here
        }
        protected async override void OnInitialized()
        {
            var list = new List<Notification>();
            LoadNotification(list);
            //return base.OnInitializedAsync();
        }

        protected async void HandleValidSearch()
        {
            Utilities.Log("=======================Notification Search Start==================");
            Loading = true;
            var app = await LocalStorageService.GetItemAsync<UserInfo>("userInfo");
            var authToken = Encoding.ASCII.GetBytes($"{app.Client.UserId}:{app.Client.UserSecret}");
            Http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(authToken));
            var clientType = app.Client.ClientType;
            if (clientType == ClientType.None)
            {
                TerminalSearch.MerchantIds = "All";
            }
            else
            {
                TerminalSearch.MerchantIds = app.Client.MerchantIds;
            }
            TerminalSearch.user = new EJournal.Domain.Entities.ClientUser
            {
                ClientType = app.Client.ClientType,
                MerchantIds = app.Client.MerchantIds,
                BankCode = app.Client.BankCode
            };
            Notification = null;
            Utilities.Log("Payload" + JsonConvert.SerializeObject(TerminalSearch));
            var list = (await Http.PostJsonAsync<Notification[]>("api/Reports/ReceitDetailsForReport", TerminalSearch)).ToList();
            notifyList = list;

            LoadNotification(notifyList);

            //await Notification.ReloadCurrentPage().ConfigureAwait(false);
            Loading = false;
            StateHasChanged();
            // Searching = false;
        }

        public void ShowDetails(string stan)
        {
            InvokeAsync(() =>
            {
                var rId = notifyList.Find(x => x.Stan == stan);
                NotificationDto = rId;
                TerminalNotificationDialog.Show();
                StateHasChanged();
            });
           
        }
        private void LoadNotification(List<Notification> list)
        {
            Notification = new CollectionTableDataAdapter<Notification>(list);
            Notification.ReloadCurrentPage();

        }
    }
}
