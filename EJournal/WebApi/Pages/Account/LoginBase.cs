﻿using Microsoft.AspNetCore.Components;
using System;
using System.Threading.Tasks;
using WebApi.Dto;
using WebApi.HttpServices;

namespace WebApi.Pages.Account
{
    public class LoginBase: ComponentBase
    {
        [Inject]
        protected AppState AppState { get; set; }
        [Inject]
        public NavigationManager navManager { get; set; }

        public LoginParameter LoginParams { get; set; } = new LoginParameter();
       public string Error { get; set; }
        public bool Submitting { get; set; } = false;

        public async Task<bool> OnSubmit()
        {
            Error = null;
            try
            {
                Submitting = true;
                await AppState.Login(LoginParams);
                if (AppState.IsLoggedIn)
                {
                    navManager.NavigateTo("/index");
                }
                else
                {
                    Error = "Invalid username or password";
                }
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                Error = "Internal server error, please contact your administrator";
               return false;
            }
            finally
            {
                Submitting = false;
            }
        }
    }
}
