﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EJournal.Application.Role;
using EJournal.Application.Role.Queries;
using EJournal.Application.User;
using EJournal.Application.User.Commands;
using EJournal.Application.User.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly IEmailSenderService _mailer;

        public AccountController(IMapper mapper, IEmailSenderService mailer)
        {
            _mapper = mapper;
            _mailer = mailer;
        }

        [HttpGet("Users")]
        public async Task<ActionResult<List<UserDto>>> GetUser()
        {
            var user = User.Claims;
            var list = await Mediator.Send(new GetUserQuery());
            return list.ToList();

        }

        [HttpGet("Roles")]
        public async Task<ActionResult<List<RoleDto>>> GetRole()
        {
            return await Mediator.Send(new GetRoleQuery());

        }
      
        [HttpPost("UserManager")]
        public async Task<ActionResult<int>> CreateOrEditUser(UserDto user)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState.Values.SelectMany(state => state.Errors)
                                                                     .Select(error => error.ErrorMessage)
                                                                     .FirstOrDefault());
                Random random = new Random();
                user.Password = user.FirstName + "@" + random.Next(9999);
                var command = _mapper.Map<UserCommand>(user);
                var rId = await Mediator.Send(command);
                if (rId.Item1 > 0)
                {
                    user.Password = rId.Item2;
                    await _mailer.SendEmailToUser(user);
                }
                return rId.Item1;
            }
            catch (Exception ex)
            {

                throw ex;
            }
         

        }

        [HttpPost("RoleManager")]
        public async Task<ActionResult<int>> CreateOrEditRole(RoleDto role)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.Values.SelectMany(state => state.Errors)
                                                                        .Select(error => error.ErrorMessage)
                                                                        .FirstOrDefault());
            var command = _mapper.Map<RoleCommand>(role);
            var rId = await Mediator.Send(command);
            return rId;

        }
    }
}