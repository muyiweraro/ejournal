﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using EJournal.Application.Client.Commands;
using EJournal.Application.Client;
using EJournal.Application.Client.Queries;
using WebApi.Services;
using Microsoft.AspNetCore.Authorization;
using EJournal.Application.Merchant.Queries;
using EJournal.Application.Merchant;

namespace WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClientUsersController : ApiController
    {
       
        private readonly IMapper _mapper;
        private readonly IEmailSenderService _mailer;

        public ClientUsersController(IMapper mapper, IEmailSenderService mailer)
        {
            _mapper = mapper;
            _mailer = mailer;
        }


        [HttpGet()]
        public async Task<ActionResult<IEnumerable<ClientDto>>> GetClientUsers()
        {
            //var query = await Mediator.Send(new GetClientQuery());
            return await Mediator.Send(new GetClientQuery()) ?? new List<ClientDto>();
        }

        [HttpGet("GetMerchantClients")]
        public async Task<ActionResult<IEnumerable<MerchantDTO>>> GetMerchantClients()  
        {
            //var query = await Mediator.Send(new GetClientQuery());
            return await Mediator.Send(new GetMerchantsQuery()) ?? new List<MerchantDTO>();  
        }

        [HttpPost("DisableClient")]
        public async Task<IActionResult> EnableDisableClient(EnableDisableCmd cmd)
        {
         var status =  await Mediator.Send(cmd);
            return Ok(status);
        }

        
        [HttpPost("Edit")]
        public async Task<ActionResult<int>> EditClientUser(ClientDto client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var command = _mapper.Map<EditClientCommand>(client);
            var rId = await Mediator.Send(command);

            return rId;
        }

        [HttpPost]
        public async Task<ActionResult<int>> PostClientUser(ClientDto client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var command = _mapper.Map<SetUpClientCommand>(client);
            var item = await Mediator.Send(command);
            if (item.Item1 > 0)
            {
                client.UserSecret = item.Item2;
                await _mailer.SendEmailToClient(client);
            }
            return item.Item1;
            //return CreatedAtAction("GetClientUser", new { id = result.Id }, result);
        }

        //// DELETE: api/ClientUsers/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<ClientUser>> DeleteClientUser(int id)
        //{
        //    var clientUser = await _context.ClientUsers.FindAsync(id);
        //    if (clientUser == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.ClientUsers.Remove(clientUser);
        //    await _context.SaveChangesAsync();

        //    return clientUser;
        //}

        //private bool ClientUserExists(int id)
        //{
        //    return _context.Clients.Any(e => e.Id == id);
        //}
    }
}
