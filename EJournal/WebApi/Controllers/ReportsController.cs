﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EJournal.Application.Notification;
using EJournal.Application.Notifications.Queries.GetNotification;
using EJournal.Domain.Entities;
using EJournal.Infrastructure.Filter;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Helpers;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ApiController
    {
        private readonly ILogger<ApiController> _logger;
       
        public ReportsController(ILogger<ApiController> logger)
        {
            _logger = logger;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [BasicAuth]
        [HttpPost("ReceitDetailsForReport")]
        //[ActionName("ReceitDetailsForReport")]
        public async Task<ActionResult<List<Notification>>> ReceitDetails(GetNotificationByReportCriteriaQuery queryCommand)
        {
            try
            {
                if (string.IsNullOrEmpty(queryCommand.EndDate) && string.IsNullOrEmpty(queryCommand.StartDate) && string.IsNullOrEmpty(queryCommand.RRN)
               && string.IsNullOrEmpty(queryCommand.TerminalId) && string.IsNullOrEmpty(queryCommand.STAN) && string.IsNullOrEmpty(queryCommand.ApprovalCode))
                {
                    var error = "Please provide the necessary fields";
                    return Ok(error);
                }
                var result = await Mediator.Send(queryCommand);
                return result;
            }
            catch (Exception ex)
            {
                Utilities.Log("Error on ReciptDetails API: " + ex.Message);
                return new List<Notification>();
            }
        }

        [BasicAuth]
        [HttpPost("ReceiptDetailsFor3Party")]
        public async Task<ActionResult<List<NotificationDto>>> ReceiptDetailsFor3Party(GetNotificationByThirdPartyQuery queryCommand)
        {
            if (string.IsNullOrEmpty(queryCommand.EndDate) && string.IsNullOrEmpty(queryCommand.StartDate) && string.IsNullOrEmpty(queryCommand.RRN)
                && string.IsNullOrEmpty(queryCommand.TerminalId) && string.IsNullOrEmpty(queryCommand.STAN) && string.IsNullOrEmpty(queryCommand.ApprovalCode))
            {
                var error = "Please provide the necessary fields";
                return Ok(error);
            }
            //return await Mediator.Send(new GetNotificationQuery());
            return await Mediator.Send(queryCommand);
            //return Ok(obj);
            //return await Mediator.Send(command);
        }
    }
}