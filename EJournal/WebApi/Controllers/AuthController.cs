﻿using EJournal.Application.User.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Dto;
using Microsoft.Extensions.Configuration;
using EJournal.Application.User;
using WebApi.Services;
using EJournal.Application.Common.Models;
using Blazored.SessionStorage;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using WebApi.Helpers;

namespace WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    public class AuthController:ApiController
    {
        private readonly IConfiguration _config;
        private readonly IEmailSenderService _mailer;
        private readonly IHttpContextAccessor _httpContext;

        public AuthController(IConfiguration config, IEmailSenderService mailer, IHttpContextAccessor httpContext)
        {
            _config = config;
            _mailer = mailer;
            _httpContext = httpContext;
        }
        [HttpPost(nameof(Login))]
        public async Task<ActionResult> Login([FromBody]LoginParameter parameter)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.Values.SelectMany(state => state.Errors)
                                                                        .Select(error => error.ErrorMessage)
                                                                        .FirstOrDefault());
           
                var user = await Mediator.Send(new LoginQuery { Username = parameter.Username, Password = parameter.Password }); ;
                if (user == null) return Unauthorized("Invalid username or password");

                var tokenString = GenerateJSONWebToken(user);
                var result = GenerateUserInfo(user, tokenString);
                _httpContext.HttpContext.Session.SetString("userName", parameter.Username);
                _httpContext.HttpContext.Session.SetString("User", JsonConvert.SerializeObject(user));
                _httpContext.HttpContext.Session.SetString("Token", tokenString);
                _httpContext.HttpContext.Session.SetString("IsLoggedIn", "true");
                var users = _httpContext.HttpContext.Session.GetString("User");
                return Ok(result);
        }



        [HttpPost(nameof(ChangePassword))]
        public async Task<ActionResult> ChangePassword([FromBody]PasswordChange parameter)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.Values.SelectMany(state => state.Errors)
                                                                        .Select(error => error.ErrorMessage)
                                                                        .FirstOrDefault());
            var passwChange = await Mediator.Send(new ChangePasswordQuery {  Password = parameter.Password, NewPassword= parameter.NewPassword }); ;
           
            return Ok(passwChange);

        }

        [HttpPost(nameof(ForgetPassword))]
        public async Task<ActionResult> ForgetPassword([FromBody]string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return Ok(false);
            }
            var passwChange = await Mediator.Send(new ForgetPasswordQuery { Username = email });
           if (passwChange.Item1)
            {
                await _mailer.SendForgetPasswordEmailToUser(email, passwChange.Item2);
            }

            return Ok(passwChange.Item1);

        }

        public UserClientDetails GenerateUserInfo(UserDto userDto, string token)
        {
            UserClientDetails userInfo = new UserClientDetails
            {
                Username = userDto.Username,
                Client = userDto.Client,
                token = token
            };
            return userInfo;
        }

        private string GenerateJSONWebToken(UserDto user)
        {
            //Hash Security Key Object from the JWT Key
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Generate list of claims with general and universally recommended claims
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.GivenName, $"{user?.FirstName} {user?.LastName}"),
                new Claim(ClaimTypes.Role, user?.RoleName),
                new Claim(ClaimTypes.Surname, user?.LastName),
                new Claim(ClaimTypes.Sid, user?.Id.ToString()),
                new Claim("ClientId", user?.ClientId.ToString())
            };

            //Generate final token adding Issuer and Subscriber data, claims, expriation time and Key
            var token = new JwtSecurityToken(_config["Jwt:Issuer"]
                , _config["Jwt:Issuer"],
                claims,
                null,
                expires: DateTime.Now.AddHours(5),
                signingCredentials: credentials
            );

            //Return token string
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
