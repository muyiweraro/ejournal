﻿using EJournal.Application.Merchant;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.HttpServices
{
    public interface IMerchantService
    {
        Task<IEnumerable<MerchantDTO>> GetMerchantAsync();
    }
}