﻿using EJournal.Application.Client;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApi.Dto;

namespace WebApi.HttpServices
{
    public interface IClientUserService
    {
      Task<IEnumerable<ClientDto>>   GetClientUsersAsync();
      Task<EJournal.Application.Merchant.MerchantDTO[]> GetMerchantClients();
      Task<int> PostClientUsersAsync(ClientDto ClientObject);
      Task<int> UpdateClientUsersAsync(ClientDto ClientObject);
    }

    public class ClientUserService : IClientUserService
    {
        private  readonly HttpClient _client;
        private readonly Domains _domains;
        private readonly string baseUrl;
        public ClientUserService(Domains domains, HttpClient client)
        {
           // IHttpContextAccessor httpContextAccessor
            _domains = domains;
            _client = client;
            baseUrl = _domains.BaseUrl;
           // _httpContextAccessor = httpContextAccessor;
          //  baseUrl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{ _httpContextAccessor.HttpContext.Request.Host.Value}";
        }
       

       public async Task<IEnumerable<ClientDto>> GetClientUsersAsync()
        {
           
            var users = await _client.GetJsonAsync<ClientDto[]>($"{baseUrl}/api/ClientUsers");
            return users;
        }

        public async Task<EJournal.Application.Merchant.MerchantDTO[]> GetMerchantClients()
        {

            var merchants = await _client.GetJsonAsync<EJournal.Application.Merchant.MerchantDTO[]>($"{baseUrl}/api/ClientUsers/GetMerchantClients");
            return merchants;
        }

        public async Task<int> PostClientUsersAsync(ClientDto ClientObject) 
        {

            var rId = await _client.PostJsonAsync<int>($"{baseUrl}/api/ClientUsers", ClientObject);
            return rId;
        }

        public async Task<int> UpdateClientUsersAsync(ClientDto ClientObject) 
        {

            var rId = await _client.PostJsonAsync<int>($"{baseUrl}/api/ClientUsers/Edit", ClientObject);
            return rId;
        }

    }
}
