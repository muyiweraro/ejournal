﻿using EJournal.Application.Merchant;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApi.Dto;

namespace WebApi.HttpServices
{
    public class MerchantService : IMerchantService
    {
        private readonly HttpClient _client;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string baseUrl;
        private readonly Domains _domains;

        public MerchantService(HttpClient client, Domains domains)
        {
            _client = client;
            _domains = domains;
            // _httpContextAccessor = httpContextAccessor;
            //  baseUrl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{ _httpContextAccessor.HttpContext.Request.Host.Value}";
            baseUrl = _domains.BaseUrl;
        }
        public async Task<IEnumerable<MerchantDTO>> GetMerchantAsync()
        {

            var users = await _client.GetJsonAsync<MerchantDTO[]>($"{baseUrl}/api/GetMerchantClients");
            return users;
        }
    }
}
