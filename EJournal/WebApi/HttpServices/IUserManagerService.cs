﻿using EJournal.Application.Role;
using EJournal.Application.User;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Dto;

namespace WebApi.HttpServices
{
    public interface IUserManagerService
    {
        Task<List<RoleDto>> GetRoleAsync();
        Task<List<UserDto>> GetUserAsync();
        Task<int> ManagerRoleAsync(RoleDto role);
        Task<int> ManagerUserAsync(UserDto user);

        Task<bool> ChangePassword(PasswordChange model);
        Task<bool> ForgetPassword(string model);
    }
}