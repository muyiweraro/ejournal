﻿using Blazored.LocalStorage;
using Blazored.SessionStorage;
using EJournal.Application.Common.Models;
using EJournal.Application.User;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebApi.Dto;
using WebApi.Helpers;

namespace WebApi.HttpServices
{
    public class AppState
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly HttpClient _httpClient;
        private const string UserInfo_SESSION_OBJECT_KEY = "UserInfo_session_obj";
        private readonly byte[] authenticationKey;
        private readonly byte[] cryptographyKey;
        private readonly ISessionStorageService _storageService;
        private readonly ISyncLocalStorageService _syncLocalStorageService;
        private readonly ILocalStorageService _localStorageService;
        private readonly NavigationManager _navManager;
        private readonly IJSInProcessRuntime _jSInProcessRuntime;
        private readonly IJSRuntime _jSRuntime;

        public bool IsLoggedIn { get;  set; }
        public bool IsAdmin { get; set; } = false;
        public bool LoggedIn { get { return true; } private set { } }

        public bool Admin { get; private set; }
        public string FullName { get; set; }

        public AppState(HttpClient httpClient,
                           IHttpContextAccessor httpContext,
                           IJSRuntime jSRuntime, 
                        // IJSInProcessRuntime jSInProcessRuntime,
                           ISyncLocalStorageService syncLocalStorageService,
                        ISessionStorageService storageService,
                        ILocalStorageService localStorageService,
                        NavigationManager navManager)
        {
            _navManager = navManager;
            _syncLocalStorageService = syncLocalStorageService;
            _httpClient = httpClient;
            _jSRuntime = jSRuntime;
            _jSInProcessRuntime = jSRuntime as IJSInProcessRuntime; //jSInProcessRuntime;
            _httpContext = httpContext;
            _storageService = storageService;
            _localStorageService = localStorageService;
            authenticationKey = AuthenticatedEncryption.AuthenticatedEncryption.NewKey();
            cryptographyKey = AuthenticatedEncryption.AuthenticatedEncryption.NewKey();
        }

        public async Task Login(LoginParameter loginDetails)
        {
            try
            {
                var url = _navManager.ToAbsoluteUri("/api/Auth/Login");
                var response = await _httpClient.PostAsync(url, new StringContent(JsonConvert.SerializeObject(loginDetails), Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                   // _httpContext.HttpContext.Session.SetString("IsLoggedIn", "true");
                    await SetUserInfoSession(response);
                    await SetAuthorizationHeader();
                    IsLoggedIn = true;
                  //  _httpContext.HttpContext.Session.SetString("IsLoggedIn", "true");
                   //  await _storageService.SetItemAsync("IsLoggedIn", "true");
                }
            }
            catch (Exception ex)
            {
                var aaa = ex.Message;
            }
         
        }

        public bool IsAdministrator() 
        {
            return false;
        }

        public UserClientDetails CheckUser(string user) 
        {
            try
            {
                UserClientDetails userInfo = JsonConvert.DeserializeObject<UserClientDetails>(user); 
                if(userInfo != null)
                {
                   return userInfo;
                }
                else
                {
                    return null;
                }
                //var user = _syncLocalStorageService.GetItem<bool>("IsAdmin");
                // var user = _jSInProcessRuntime.Invoke<bool>("Blazored.LocalStorage.GetItem", "IsAdmin");
               
            }
            catch (Exception ex)
            {
                var ss = ex.Message;
                return null;
            }
          

            
        }

        //public async  Task<bool> CheckSession() 
        //{
        //    try
        //    {
        //        var user = _httpContext.HttpContext.Session.GetString("User");
        //        var deserializedUser = JsonConvert.DeserializeObject<UserDto>(user);
        //        FullName = deserializedUser.FirstName + " " +  deserializedUser.LastName;
        //        IsLoggedIn = true;
        //        string token = _httpContext.HttpContext.Session.GetString("IsLoggedIn");
        //        SetAuthorization(token);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public async Task<bool> CheckLoginSession()
        //{
        //    try
        //    {
        //        var loginSession = await _storageService.GetItemAsync<object>("IsLoggedIn");
        //        var result = Convert.ToBoolean(loginSession.ToString());
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public async Task Logout()
        {
            await _storageService.RemoveItemAsync(UserInfo_SESSION_OBJECT_KEY);
 
             IsLoggedIn = false;
            _navManager.NavigateTo("/login");
        }

        public async Task<string> GetUserTokenSession()
        {
            string encryptedObj = await _storageService.GetItemAsync<string>(UserInfo_SESSION_OBJECT_KEY);

            // no active UserInfo session found!
            if (string.IsNullOrEmpty(encryptedObj))
            {
                await Logout();
                return null;
            }


            //string decryptedObj = AuthenticatedEncryption.AuthenticatedEncryption.Decrypt(encryptedObj, cryptographyKey, authenticationKey);
            return encryptedObj;
          
        }
        private async Task SetUserInfoSession(HttpResponseMessage response)
        {
            var responseContent = await response.Content.ReadAsStringAsync();
             var user = JsonConvert.DeserializeObject<UserClientDetails>(responseContent);
            Utilities.Log("token " + JsonConvert.SerializeObject(user));
            // SetAuthorization(responseContent);
            SetAuthorization(user.token);
            await _storageService.SetItemAsync("IsLogIn", true);
            await _localStorageService.SetItemAsync("userInfo", user);
            var serializedUser = JsonConvert.SerializeObject(user);
           // _httpContext.HttpContext.Session.SetString("User", serializedUser);
            //await _localStorageService.SetItemAsync(UserInfo_SESSION_OBJECT_KEY,
            //  AuthenticatedEncryption.AuthenticatedEncryption.Encrypt(user.token, cryptographyKey, authenticationKey));
            //await _storageService.SetItemAsync(UserInfo_SESSION_OBJECT_KEY,
            //AuthenticatedEncryption.AuthenticatedEncryption.Encrypt(user.token, cryptographyKey, authenticationKey));
            await _storageService.SetItemAsync(UserInfo_SESSION_OBJECT_KEY, user.token);

        }
        private void SetAuthorization(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;
            var fullName = tokenS.Claims.FirstOrDefault(claim => claim.Type == JwtRegisteredClaimNames.GivenName).Value;
            var role = tokenS.Claims.FirstOrDefault(claim => claim.Type == System.Security.Claims.ClaimTypes.Role).Value;
            if (role.Equals("Administrator"))
            {
                IsAdmin = true;
               // _httpContext.HttpContext.Session.SetString("IsAdmin", "true");
                _localStorageService.SetItemAsync("IsAdmin", true);
            }
            else
            {
                IsAdmin = false;
                _localStorageService.SetItemAsync("IsAdmin", false);
            }
            FullName = fullName ?? string.Empty;
        }
        private async Task SetAuthorizationHeader()
        {
            if (!_httpClient.DefaultRequestHeaders.Contains("Authorization"))
            {
                var token = await GetUserTokenSession();
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
               
            }
        }
    }
}
