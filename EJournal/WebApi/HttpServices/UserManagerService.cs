﻿using EJournal.Application.Role;
using EJournal.Application.User;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebApi.Dto;

namespace WebApi.HttpServices
{
    public class UserManagerService : IUserManagerService
    //: IAuthService
    {
        private readonly HttpClient _client;
        private readonly NavigationManager _navigationManager;
        private readonly AppState _appState;
        public UserManagerService(HttpClient client, NavigationManager navigationManager, AppState appState)
        {
            _client = client;
            _navigationManager = navigationManager;
            _appState = appState;

        }

        public async Task<List<UserDto>> GetUserAsync()
        {
            var url = _navigationManager.ToAbsoluteUri("/api/Account/Users");
            var result = await _client.GetJsonAsync<List<UserDto>>(url.ToString());

            return result;
        }

        public async Task<int> ManagerUserAsync(UserDto user)
        {
            var url = _navigationManager.ToAbsoluteUri("/api/Account/UserManager").ToString();
            await SetAuthorizationHeader();
            var result = await _client.PostJsonAsync<int>(url, user);
            return result;
        }

        public async Task<List<RoleDto>> GetRoleAsync()
        {
            var url = _navigationManager.ToAbsoluteUri("/api/Account/Roles").ToString();
            await SetAuthorizationHeader();
            var result = await _client.GetJsonAsync<List<RoleDto>>(url);

            return result;
        }

        public async Task<int> ManagerRoleAsync(RoleDto role)
        {
            var url = _navigationManager.ToAbsoluteUri("/api/Account/RoleManager").ToString();
            await SetAuthorizationHeader();
            var result = await _client.PostJsonAsync<int>(url, role);
            return result;
        }
        public async Task<bool> ChangePassword(PasswordChange model)
        {
            var url = _navigationManager.ToAbsoluteUri("/api/auth/ChangePassword").ToString();
            var result = await _client.PostJsonAsync<bool>(url, model);
            return result;
        }
        public async Task<bool> ForgetPassword(string model)
        {
            var url = _navigationManager.ToAbsoluteUri("/api/auth/ForgetPassword").ToString();
            var result = await _client.PostJsonAsync<bool>(url, model);
            return result;
        }
        private async Task SetAuthorizationHeader()
        {
            var token = await _appState.GetUserTokenSession();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }
    }
}
