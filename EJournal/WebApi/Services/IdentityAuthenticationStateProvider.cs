﻿using EJournal.Application;
using EJournal.Application.Client;
using EJournal.Application.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.Dto;
using WebApi.HttpServices;

namespace WebApi.Services
{
    public class IdentityAuthenticationStateProvider : AuthenticationStateProvider
    {
        private AppUser _userInfoCache;
        private ClientDto _clientInfoCache;
        private readonly IIdentityService _identityService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IAuthService _authService;

        public IdentityAuthenticationStateProvider(IIdentityService identityService, IHttpContextAccessor httpContextAccessor, IAuthService authService)
        {
            _identityService = identityService;
            _httpContextAccessor = httpContextAccessor;
            _authService = authService;
        }
        public async Task Login(LoginParameter loginParams)
        {
           await _authService.Login(loginParams);
           
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }
        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var identity = new ClaimsIdentity();
            var (appUser, client) = await GetUserInfo();
            try
            {
                if (appUser != null)
                {
                    var claims = new[] {
                new Claim(ClaimTypes.Name,appUser.Username ?? string.Empty),
                new Claim(ClaimTypes.Role,appUser.RoleName ?? string.Empty),
                new Claim("FirstName",appUser.FirstName ?? string.Empty),
                new Claim("LastName",appUser.LastName ?? string.Empty),
                new Claim("MerchantIds",client.MerchantIds ?? string.Empty),
                new Claim("BankIds",client.BankCode ?? string.Empty),
                new Claim("ClientName",client.ClientName ?? string.Empty),
                new Claim("ClientType",client.ClientType.ToString()),
                new Claim("AppID",client.UserId ?? string.Empty),
                new Claim("AppKey",client.UserSecret ?? string.Empty)
            };
                    identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                }
                //var principal = new ClaimsPrincipal(identity);
               // await _httpContextAccessor.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
            }
            catch (Exception ex)
            {

                throw;
            }
            //await HttpContext.SignInAsync(
            //    CookieAuthenticationDefaults.AuthenticationScheme, principal);
            return new AuthenticationState(new ClaimsPrincipal(identity));
        }

        private async Task<(AppUser appUser, ClientDto client)> GetUserInfo()
        {
            if (_userInfoCache != null && _clientInfoCache != null)
            {
                return (_userInfoCache, _clientInfoCache);
            }
            if (_userInfoCache != null && _clientInfoCache == null)
            {
                return (_userInfoCache, new ClientDto());
            }
            else
            {
                var (a, b) = await _authService.GetUserInfo();
                _userInfoCache = a;
                _clientInfoCache = b;
                return (_userInfoCache, _clientInfoCache);
            }

           // return await Task.FromResult((new AppUser(), new ClientDto()));
        }
    }
}
