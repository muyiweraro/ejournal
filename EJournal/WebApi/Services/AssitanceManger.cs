﻿using Blazored.LocalStorage;
using EJournal.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services
{
    public class AssitanceManager
    {
        private readonly ILocalStorageService _localStorageService;

        public AssitanceManager()
        {
        }

        public AssitanceManager(ILocalStorageService localStorageService)
        {
            _localStorageService = localStorageService;
        }

        public async Task ClearLocalStorage()
        {
            var find = await _localStorageService.LengthAsync();
            var app = await _localStorageService.GetItemAsync<UserInfo>("userInfo");
        }
    }
}
