﻿using MimeKit;
using WebApi.Dto;
using System.Threading.Tasks;
using EJournal.Application.User;
using System;
using EJournal.Application.Client;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;

namespace WebApi.Services
{
    public interface IEmailSenderService
    {
        void SendEmail(Message message);
        Task SendEmailToUser(UserDto message);
        Task SendForgetPasswordEmailToUser(string email,string password);
        Task SendEmailToClient(ClientDto message);
        Task SendEmailAsync(Message message);
    }

    public class EmailSenderService : IEmailSenderService
    {
        private readonly EmailConfiguration _emailConfig;
        private readonly Domains _domains;
        // public readonly NavigationManager _navigationManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public EmailSenderService(Domains domains,EmailConfiguration emailConfig)
        {
            _domains = domains;
            _emailConfig = emailConfig;
           // _httpContextAccessor = httpContextAccessor;
        }

        public void SendEmail(Message message)
        {
            var emailMessage = CreateEmailMessage(message);

            Send(emailMessage);
        }
        public async Task SendEmailToUser(UserDto user)
        {
            var msg = $"Hello {user.FirstName},{Environment.NewLine} Find below your credentails for eJournal login" +
                $"{Environment.NewLine}{Environment.NewLine}Username: {user.Username} {Environment.NewLine}Password: {user.Password}." +
                $"{Environment.NewLine}Please note it is advisable you change your  password when accessing eJournal for the first time";
           
            var snd = CreateEmailMessage(new Message(user.Username, "User credentails", msg));
            await  SendAsync(snd);


        }

        public async Task SendEmailToClient(ClientDto user)
        {
           // var value = _httpContextAccessor.HttpContext.Request;
            // var url = $"{value.Scheme}://{value.Host}/api/Reports/ReceiptDetailsFor3Party";
            var url = _domains.BaseUrl + "/api/Reports/ReceiptDetailsFor3Party";
            var swaggerDoc= $"{_domains.BaseUrl}/";
            var subject = "Xpress eJournal Setup Notification!!!";
            
           var body = $"Dear {user.ClientName},{Environment.NewLine}{Environment.NewLine} " +
                $"Your have been profile on our platform.{Environment.NewLine} " +
                $"Kindly access the API using Basic authentication with the parameters stated below;{Environment.NewLine}{Environment.NewLine}" +
                $"Username: {user.ClientName} { Environment.NewLine}Password: {user.UserSecret} {Environment.NewLine}Url: {url}" +
                $"{Environment.NewLine}Your can access the API Documentation on {swaggerDoc}" +
                $"{Environment.NewLine}{Environment.NewLine}Best Regards,{Environment.NewLine} Xpress Payments LTD";
            var snd = CreateEmailMessage(new Message(user.ClientEmail, subject, body));
            await SendAsync(snd);
        }

        public async Task SendForgetPasswordEmailToUser(string email, string password)
        {
            var subject = "Forget Password";
            var body = $"Dear {email.Split('@')[0]} {Environment.NewLine}Below is a code/password for you to reset/change your password.{Environment.NewLine}{password}{Environment.NewLine}";
            var snd = CreateEmailMessage(new Message(email, subject, body));
            await SendAsync(snd);
        }
        private MimeMessage CreateEmailMessage(Message message)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_emailConfig.From));
            emailMessage.To.Add(new MailboxAddress(message.To));
            emailMessage.Subject = message.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Text) { Text = message.Content };
            return emailMessage;
        }

        private void Send(MimeMessage mailMessage)
        {
            using var client = new MailKit.Net.Smtp.SmtpClient();
            try
            {
                client.Connect(_emailConfig.SmtpServer, _emailConfig.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(_emailConfig.UserName, _emailConfig.Password);

                client.Send(mailMessage);
            }
            catch
            {
                //log an error message or throw an exception or both.
                throw;
            }
            finally
            {
                client.Disconnect(true);
                client.Dispose();
            }
        }

        private async Task SendAsync(MimeMessage mailMessage)
        {
            using var client = new MailKit.Net.Smtp.SmtpClient();
            try
            {
                await client.ConnectAsync(_emailConfig.SmtpServer, _emailConfig.Port, MailKit.Security.SecureSocketOptions.StartTlsWhenAvailable);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(_emailConfig.UserName, _emailConfig.Password);

                await client.SendAsync(mailMessage);
            }
            catch (Exception ex)
            {
                //log an error message or throw an exception, or both.
               // throw;
            }
            finally
            {
                await client.DisconnectAsync(true);
                client.Dispose();
            }
        }

        public Task SendEmailAsync(Message message)
        {
            throw new System.NotImplementedException();
        }

       
    }
}
