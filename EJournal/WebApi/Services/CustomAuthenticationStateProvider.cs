﻿using Blazored.LocalStorage;
using EJournal.Application.Common.Models;
using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.HttpServices;

namespace WebApi.Services
{
    public class CustomAuthenticationStateProvider : AuthenticationStateProvider, IAuthFeatures
    {
        public ILocalStorageService _localStorageService { get; }
        UserInfo GetUserInfo;
        public CustomAuthenticationStateProvider(ILocalStorageService localStorageService)
        {
            //_authService = authService;
            _localStorageService = localStorageService;
        }
        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var accessToken = await _localStorageService.GetItemAsync<string>("username");

            ClaimsIdentity identity;

            if (accessToken != null && accessToken != string.Empty)
            {
                UserInfo user = GetUserInfo;//await _authService.GetUserInfo(accessToken);
                identity = GetClaimsIdentity(user);
            }
            else
            {
                identity = new ClaimsIdentity();
            }

            var claimsPrincipal = new ClaimsPrincipal(identity);

            return await Task.FromResult(new AuthenticationState(claimsPrincipal));
        }

        public async Task MarkUserAsAuthenticated(UserInfo user)
        {
            await _localStorageService.SetItemAsync("appId", user?.Client?.UserId);
            await _localStorageService.SetItemAsync("appSecret", user?.Client?.UserSecret);
            await _localStorageService.SetItemAsync("username", user?.Username);
            await _localStorageService.SetItemAsync("userInfo", user);
            GetUserInfo = user;
            var identity = GetClaimsIdentity(user);

            var claimsPrincipal = new ClaimsPrincipal(identity);

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(claimsPrincipal)));
        }

        public async Task MarkUserAsLoggedOut()
        {
            await _localStorageService.RemoveItemAsync("appId");
            await _localStorageService.RemoveItemAsync("appId");

            var identity = new ClaimsIdentity();

            var user = new ClaimsPrincipal(identity);

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(user)));
        }

        private ClaimsIdentity GetClaimsIdentity(UserInfo user)
        {
            var claimsIdentity = new ClaimsIdentity();

            if (user.Username != null)
            {
                claimsIdentity = new ClaimsIdentity(new[]
                                {
                new Claim(ClaimTypes.Name,user.Username ?? string.Empty),
                new Claim(ClaimTypes.Role,user.RoleName ?? string.Empty),
                new Claim("FirstName",user.FirstName ?? string.Empty),
                new Claim("LastName",user.LastName ?? string.Empty),
                new Claim("MerchantIds",user?.Client.MerchantIds ?? string.Empty),
                new Claim("BankIds",user?.Client.BankCode ?? string.Empty),
                new Claim("ClientName",user?.Client.ClientName ?? string.Empty),
                new Claim("ClientType",user?.Client.ClientType.ToString()),
                new Claim("AppID",user?.Client.UserId ?? string.Empty),
                new Claim("AppKey",user?.Client.UserSecret ?? string.Empty)
                                }, "apiauth_type");
            }

            return claimsIdentity;
        }
    }
}
