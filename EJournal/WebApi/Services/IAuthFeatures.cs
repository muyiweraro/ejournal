﻿using EJournal.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services
{
    public interface IAuthFeatures
    {
        Task MarkUserAsAuthenticated(UserInfo userInfo);

        Task MarkUserAsLoggedOut();
    }
}
