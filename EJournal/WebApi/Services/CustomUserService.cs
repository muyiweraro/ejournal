﻿using Blazored.SessionStorage;
using EJournal.Application.Common.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
namespace WebApi.Services
{
    public class CustomUserService : ServerAuthenticationStateProvider
    {
        private const string UserInfo_SESSION_OBJECT_KEY = "UserInfo_session_obj";

        private byte[] authenticationKey, cryptographyKey;
        private ISessionStorageService storageService;

        public CustomUserService(ISessionStorageService storageService)
        {
            this.storageService = storageService;

            authenticationKey = AuthenticatedEncryption.AuthenticatedEncryption.NewKey();
            cryptographyKey = AuthenticatedEncryption.AuthenticatedEncryption.NewKey();
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            // read a possible UserInfo session object from the storage.
            UserInfo UserInfoSession = await GetUserInfoSession();

            if (UserInfoSession != null)
                return await GenerateAuthenticationState(UserInfoSession);
            return await GenerateEmptyAuthenticationState();
        }

        public async Task LoginAsync(UserInfo UserInfo)
        {
            // store the session information in the client's storage.
            await SetUserInfoSession(UserInfo);

            // notify the authentication state provider.
            NotifyAuthenticationStateChanged(GenerateAuthenticationState(UserInfo));
        }

        public async Task LogoutAsync()
        {
            // delete the UserInfo's session object.
            await SetUserInfoSession(null);

            // notify the authentication state provider.
            NotifyAuthenticationStateChanged(GenerateEmptyAuthenticationState());
        }

        public async Task<UserInfo> GetUserInfoSession()
        {
            string encryptedObj = await storageService.GetItemAsync<string>(UserInfo_SESSION_OBJECT_KEY);

            // no active UserInfo session found!
            if (string.IsNullOrEmpty(encryptedObj))
                return null;

            string decryptedObj = AuthenticatedEncryption.AuthenticatedEncryption.Decrypt(encryptedObj, cryptographyKey, authenticationKey);

            try
            {
                return JsonConvert.DeserializeObject<UserInfo>(decryptedObj);
            }
            catch
            {
                // UserInfo could have modified to local value, leading to an
                // invalid decrypted object. Hence, the UserInfo just did destory
                // his own session object. We need to clear it up.
                await LogoutAsync();
                return null;
            }
        }

        private ClaimsIdentity GetClaimsIdentity(UserInfo user)
        {
            var claimsIdentity = new ClaimsIdentity();

            if (user.Username != null)
            {
                claimsIdentity = new ClaimsIdentity(new[]
                                {
                new Claim(ClaimTypes.Name,user.Username ?? string.Empty),
                new Claim(ClaimTypes.Role,user.RoleName ?? string.Empty),
                new Claim("FirstName",user.FirstName ?? string.Empty),
                new Claim("LastName",user.LastName ?? string.Empty),
                new Claim("MerchantIds",user?.Client.MerchantIds ?? string.Empty),
                new Claim("BankIds",user?.Client.BankCode ?? string.Empty),
                new Claim("ClientName",user?.Client.ClientName ?? string.Empty),
                new Claim("ClientType",user?.Client.ClientType.ToString()),
                new Claim("AppID",user?.Client.UserId ?? string.Empty),
                new Claim("AppKey",user?.Client.UserSecret ?? string.Empty)
                                }, "apiauth_type");
            }

            return claimsIdentity;
        }
        private async Task SetUserInfoSession(UserInfo UserInfo) => await storageService.SetItemAsync(UserInfo_SESSION_OBJECT_KEY,
            AuthenticatedEncryption.AuthenticatedEncryption.Encrypt(JsonConvert.SerializeObject(UserInfo), cryptographyKey, authenticationKey));

        private Task<AuthenticationState> GenerateAuthenticationState(UserInfo UserInfo)
        {
            var claimsIdentity = GetClaimsIdentity(UserInfo);
            
            ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
            return Task.FromResult(new AuthenticationState(claimsPrincipal));
        }

        private Task<AuthenticationState> GenerateEmptyAuthenticationState() => Task.FromResult(new AuthenticationState(new ClaimsPrincipal()));
    }
}
