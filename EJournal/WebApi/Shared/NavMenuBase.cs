﻿using Blazored.SessionStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.HttpServices;

namespace WebApi.Shared
{
    public class NavMenuBase : ComponentBase
    {
        [Inject]
        protected AppState AppState { get; set; }
        [Inject]
        private  IJSRuntime JS { get; set; }

         protected bool collapseNavMenu = true;
        protected bool IsLogin = true; 

        protected string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        protected void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;

        }


        protected async override Task OnInitializedAsync()
        {
            var user = await JS.InvokeAsync<string>("GetUser");
            var isLogedIn = await JS.InvokeAsync<string>("IsLogIn");
            var isAdmin = await JS.InvokeAsync<string>("IsAdmin");
            var checkUser = AppState.CheckUser(user);
            if (Convert.ToBoolean(isLogedIn) == true)
            {
                if (user != null)
                {
                    AppState.FullName = checkUser.Username;
                    AppState.IsLoggedIn = true;
                    AppState.IsAdmin = Convert.ToBoolean(isAdmin);
                }
            }
        }
    }
}
