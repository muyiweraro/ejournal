﻿using EJournal.Application.Notification;
using Microsoft.AspNetCore.Components;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using System;
using EJournal.Domain.Entities;

namespace WebApi.Components
{
    public class TerminalNotificationDialogBase:ComponentBase
    {
        public bool ShowDialog { get; set; }
       
        [Parameter]
        public Notification Notification { get; set; }


        //protected override  void OnInitialized()
        //{
        //    System.Threading.Timer timer = new System.Threading.Timer(x =>
        //    {
        //        InvokeAsync(() =>
        //        {
        //            StateHasChanged();
        //        });
        //    }, null, 1000, 1000);

        //}
        public void Show()
        {
            try
            {
                ShowDialog = true;
                //InvokeAsync(() =>
                //{
                //    ShowDialog = true;
                //    //StateHasChanged();
                //});
               
            }
            catch (Exception ex)
            {
            }
           
        }
        public void Close()
        {
            Notification = new Notification();
            ShowDialog = false;
            StateHasChanged();
        }


        public void DownloadPDF(IJSRuntime js, Notification notificationDto)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='container'>");
            sb.Append("<div class='row d-flex justify-content-center align-items-center modal-h-w' id='pdfPrintArea'>");
            sb.Append(" <div id='templateHolder'>");

            sb.Append("<table cellspacing='0' border='0' class='ws'>");
            sb.Append(@"<colgroup width='104'></colgroup>
                                    <colgroup width = '105' ></ colgroup>
                                    <colgroup span = '4' width = '94' ></ colgroup>
                                    <colgroup width = '171' ></ colgroup>
                                    <colgroup width = '104'></colgroup>");
            sb.Append("<tr>");
            sb.Append(@"<td colspan='6' height='19' align='center' valign='bottom' bgcolor='#EDEDED'>
                    <font color='#000000'><span id='institution'> " + notificationDto.MerchantName + @" </span></font></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append(@"<td colspan='6' height='19' align='center' valign='bottom' bgcolor='#EDEDED'>
                    <font color='#000000'><span id='institutionAddress'> " + notificationDto.MerchantAddress + @" </span></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pdate'>" + notificationDto.Transactiondate +
                       @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Timelocaltransaction +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />TERMINAL ID</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Terminalid +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />MERCHANT ID</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.MerchantId +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append(@"  <tr>
                           <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>
                           <td colspan = '4' align = 'center' valign = 'bottom' bgcolor = '#EDEDED' >
                           <font color = '#000000' > ***************************************************</ font ></ td >
                           <td align = 'left' valign = 'bottom' bgcolor = '#EDEDED' >< font color = '#000000' >< br /></ font ></td>
                           </tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='10' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td style='border-bottom: 1px solid #000000' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Trantype +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='10' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td class='td-1' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.TStatus +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='10' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td style='border-bottom: 1px solid #000000' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'>" + "***CUSTOMER COPY***" +
                      @"</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />PAN</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Pan +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />EXPIRY</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Track2 +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />CUSTOMER NAME</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.CustomerName +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />BATCH NO</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Batchno +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />SEQ No</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Seqno +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />STAN</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Stan +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />RRN</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Rrn +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />AUTH ID</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Aurhorisationresponse +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />AMOUNT</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Amount +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />CCOUNT</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + "DEFAULT"+
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td class='td-1' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.TStatus +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='5' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td style='border-bottom: dashed #000000' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Responsedescription +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />AID</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Aid +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />CARD</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Card +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />AC</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Ac +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />TVR</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Tvr +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br />TSI</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td align='right' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Tsi +
                      @"</span></font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td class='td-1' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + notificationDto?.Footer +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td class='td-1' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + "***************************************************" +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td class='td-1' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + "***eJOURNAL***" +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td class='td-1' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + "<a href='http://www.xpresspayments.com/'>www.xpresspayments.com</a>" +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append(" <td height='19' align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append(@"<td class='td-1' colspan='4' align='center' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><span id='pTime'>" + "23416312430" +
                      @"</span>***</font></td>");
            sb.Append(" <td align='left' valign='bottom' bgcolor='#EDEDED'><font color='#000000'><br /></font></td>");
            sb.Append("</tr>");

            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            StringReader sr = new StringReader(sb.ToString());

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
          //  HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                //htmlparser.Parse(sr);
                pdfDoc.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                js.InvokeVoidAsync("jsSaveAsFile",
                                "terminal-notification.pdf",
                                Convert.ToBase64String(bytes)
                                );
                //_httpContextAccessor.HttpContext.Response.Clear();
                //_httpContextAccessor.HttpContext.Response.ContentType = "application/pdf";
                //_httpContextAccessor.HttpContext.Response.Headers.Add("Content-Disposition", "attachment; filename=Invoice.pdf");
                //_httpContextAccessor.HttpContext.Response.Body.WriteAsync(bytes);
                //_httpContextAccessor.HttpContext.Response.Body.WriteAsync(bytes);
                //_httpContextAccessor.HttpContext.Response.Body.Close();

            }
        }
    }
}
