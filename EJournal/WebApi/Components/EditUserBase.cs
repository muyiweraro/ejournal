﻿using Blazored.Modal;
using Blazored.Modal.Services;
using Blazored.Toast.Services;
using EJournal.Application.Role;
using EJournal.Application.User;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.HttpServices;

namespace WebApi.Components
{
    public class EditUserBase : ComponentBase
    {
        [Inject]
        public IUserManagerService UserManagerService { get; set; }
        [Inject]
        public IClientUserService ClientService { get; set; }
        [Inject]
        public IToastService ToastService { get; set; }

        public UserDto UserDto { get; set; } = new UserDto();
        protected List<RoleDto> RoleList { get; set; } = new List<RoleDto>();
        protected List<ClientOption> ClientList { get; set; } = new List<ClientOption>();
        [CascadingParameter]
        protected BlazoredModalInstance BlazoredModal { get; set; }
        [Parameter]
        public string UserInfo { get; set; } 

        public string Error { get; set; } = string.Empty;
        public bool Submitting { get; set; } = false;
        protected  async override Task OnInitializedAsync()
        {
            if (!string.IsNullOrEmpty(UserInfo))
            {
                var json = JsonConvert.DeserializeObject<UserDto>(UserInfo);
                UserDto = json;
            };
            var roles = await UserManagerService.GetRoleAsync();
            RoleList = roles ?? new List<RoleDto>();
             var clients = await ClientService.GetClientUsersAsync();
          
            if(clients != null)
            {
                ClientList = clients.Select(s => new ClientOption
                {
                    Id = s.Id,
                    Name = s.ClientName
                }).ToList();
            }
           
            }

        protected async void HandleValidSubmit()
        {
            Submitting = true;
            try
            {
                var rId = await UserManagerService.ManagerUserAsync(UserDto);
                if (rId == -2)
                {
                    Error = $"User {UserDto.Username} already exist";

                    StateHasChanged();
                }
                else if (rId > 0)
                {
                    BlazoredModal.Close(ModalResult.Ok($"Record was submitted successfully."));
                }
                else
                {
                    BlazoredModal.Close(ModalResult.Ok($"Record updated successfully."));
                }
            }
            catch
            {
                Error = $"Internal server error! please try again later";
            }
            finally
            {

                Submitting = false;
            }
            
        }

      
        protected class ClientOption
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

    }
}
