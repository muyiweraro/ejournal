﻿//using Blazored.Modal;
//using Blazored.Modal.Services;
//using EJournal.Application.Client;
//using Microsoft.AspNetCore.Components;
//using Microsoft.AspNetCore.Components.Web;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Threading.Tasks;
//using WebApi.Dto;
//using WebApi.HttpServices;
//using static System.Net.WebRequestMethods;

//namespace WebApi.Components
//{
//    public class ClientDetailsBase : ComponentBase
//    {
//        [CascadingParameter]
//        BlazoredModalInstance BlazoredModal { get; set; }

//        [Parameter]
//        public string ClientInfo { get; set; }
//        //public ClientDto ClientObject { get; set; } = new ClientDto();
//        [Inject]
//        public IClientUserService ClientService { get; set; }

//        // public IMerchantService MerchantService { get; set; }
//        protected List<MerchantOption> MerchantList { get; set; } = new List<MerchantOption>();
//        public EJournal.Domain.Enums.ClientType CType { get; set; }
//        public string Error { get; set; } = string.Empty;
//        public bool Submitting { get; set; }
       

//        protected async override void OnInitialized()
//        {

//            if (!string.IsNullOrEmpty(ClientInfo))
//            {
//                var json = JsonConvert.DeserializeObject<ClientDto>(ClientInfo);
//                ClientObject = json;
//            };

//            var merchants = await ClientService.GetMerchantClients();        //await MerchantService.GetMerchantAsync();

//            if (merchants != null)
//            {
//                MerchantList = merchants.Select(s => new MerchantOption
//                {
//                    Id = s.Code,
//                    Name = s.Name
//                }).ToList();
//            }
//        }

//        protected async void HandleValidSubmit()
//        {
//            Submitting = true;
//            if (ClientObject.Id == 0)
//            {
//                var rId = await ClientService.PostClientUsersAsync(ClientObject); 
//                if (rId == -2)
//                {
//                    Error = $"Client with userId: {ClientObject.UserId} and email : {ClientObject.ClientEmail} already exist";

//                    StateHasChanged();
//                }
//                else
//                {

//                    BlazoredModal.Close(ModalResult.Ok($"Record was submitted successfully."));
//                }


//            }
//            else
//            {
//                var rId = ClientService.UpdateClientUsersAsync(ClientObject);
//                BlazoredModal.Close(ModalResult.Ok($"Record updated successfully."));
//            }

//            Submitting = false;
//        }

//        protected void CloseModal()
//        {
//            BlazoredModal.Cancel();
//        }
//       protected void OnClientTypeSelect(ChangeEventArgs e)
//        {
//            switch (e.Value.ToString())
//            {
//                case "1":
//                    // Statements executed if expression(or variable) = value1
//                    break;
//                case "2":
//                    // Statements executed if expression(or variable) = value1
//                    break;
//                    //default: 0
//            }

//            // StateHasChanged();
//        }

//        protected void OnClientTypeSelect1(MouseEventArgs e)
//        {

//        }

//        protected string _message = "Select a button to learn its position.";

//        protected void UpdateHeading(MouseEventArgs e, int buttonNumber)
//        {
//            _message = $"You selected Button #{buttonNumber} at " +
//                $"mouse position: {e.ClientX} X {e.ClientY}.";
//        }
//        //protected class MerchantOption
//        //{
//        //    public string Id { get; set; }
//        //    public string Name { get; set; }
//        //}
//    }
//}
