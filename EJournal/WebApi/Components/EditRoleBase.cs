﻿using Blazored.Modal;
using Blazored.Modal.Services;
using EJournal.Application.Role;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System.Collections.Generic;
using WebApi.HttpServices;

namespace WebApi.Components
{
    public class EditRoleBase : ComponentBase
    {
        [CascadingParameter]
        protected BlazoredModalInstance BlazoredModal { get; set; }

        [Parameter]
        public string RoleInfo { get; set; }
        [Inject]
        public IUserManagerService UserManagerService { get; set; }
        public RoleDto RoleDto { get; set; } = new RoleDto();
        protected List<RoleDto> RoleList { get; set; } = new List<RoleDto>();
        public bool Submitting { get; set; } = false;
        public string Error { get; set; }

        protected override void OnInitialized()
        {
            if (!string.IsNullOrEmpty(RoleInfo))
            {
                var json = JsonConvert.DeserializeObject<RoleDto>(RoleInfo);
                RoleDto = json;
            };
        }

        protected async void HandleValidSubmit()
        {
            Submitting = true;
            try
            {
                var id = RoleDto.Id;
                var rId = await UserManagerService.ManagerRoleAsync(RoleDto);
                if (rId > 0 && id == 0)
                {
                    BlazoredModal.Close(ModalResult.Ok($"Record was submitted successfully."));
                }
                else if (rId > 0 && id > 0)
                {
                    BlazoredModal.Close(ModalResult.Ok($"Record updated successfully."));
                }
                else
                {
                    BlazoredModal.Close(ModalResult.Ok($"Can not this operation at this time ! please try again later"));
                }
            }
            catch
            {
                Error = $"Internal server error! please try again later";
            }
            finally
            {

                Submitting = false;
            }
           
        }
       
    }
}
