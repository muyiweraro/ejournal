﻿using Coravel.Mailer.Mail;
using EJournal.Application.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Helpers
{
    public class NewUserViewMailable : Mailable<UserDto>
    {
        private UserDto _user;

        public NewUserViewMailable(UserDto user) => _user = user;

        public override void Build()
        {
            this.To(_user.Username)
                .From("from@test.com")
                .Html($"<html><body><h1>Welcome {_user.Username}</h1>" +
                $"<p>Find below your ejournal credentials <br>" +
                $"Username: {_user.Username} <br>" +
                $"Password: {_user.Password} <br>" +
                $"Best Regards" +
                $"</p></body></html>");
        }
    }
}
