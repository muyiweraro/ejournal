﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace WebApi.Helpers
{
    public static class Utilities
    {
        /// <summary>
        /// Converts hex string to byte array
        /// https://stackoverflow.com/questions/311165/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-and-vice-versa
        /// </summary>
        public static byte[] StringToByteArray(string hex)
        {
            hex = hex.Trim().Replace(" ", string.Empty); // modification
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        /// <summary>
        /// Converts byte array to hex string
        /// https://stackoverflow.com/questions/311165/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-and-vice-versa
        /// </summary>
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString().ToUpper();
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "XPEA05102020RESS";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "XPEA05102020RESS";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static void Log(string message)
        {//NO for Neft Outward
            string path = AppDomain.CurrentDomain.BaseDirectory;

            try
            {
                if (path.Trim() != "")	//there may be instances where logging to file may not be possible or desirable
                {

                    int maxSize = 204800;       //default max log size 200KB


                    //  path = path + "\\EventLog4NO.txt";
                    path = @"C:\EjournalLogs";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    path = @"C:\EjournalLogs\log.txt";
                    FileInfo fInfo = new FileInfo(path);



                    if (message.Equals("clearAll"))
                    {
                        //File.WriteAllText(path, String.Empty);
                        using (var sr = new StreamWriter(path, true))
                            sr.WriteLine(String.Empty);
                    }
                    else
                    {
                        string logtxt = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + ": " + message;
                        // File.AppendAllText(path+"\\EventLogIBS", logtxt + Environment.NewLine);
                        using (var sr = new StreamWriter(path, true))
                            sr.WriteLine(logtxt);

                    }
                    if (fInfo.Exists)
                    {
                        if (fInfo.Length >= maxSize)
                        {
                            var fname = fInfo.FullName.Replace(fInfo.Extension, "");

                            fInfo.MoveTo(fname + "_" + DateTime.Now.ToString("dd_MM_yyyy_h_mm_ss_tt") + ".txt");
                        }
                    }

                }
            }
            catch { }
        }



    }
}
