﻿using EJournal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dto
{
    public class TerminalSearchModel
    {
        public string RRN { get; set; }
        public string STAN { get; set; }
        public string ApprovalCode { get; set; }
        public string TerminalId { get; set; }
        public string MerchantIds { get; set; }
        public string BandkCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public ClientUser user { get; set; }
    }
}
