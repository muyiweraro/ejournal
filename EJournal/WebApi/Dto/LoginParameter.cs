﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Dto
{
    public class LoginParameter
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
