﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Dto
{
    public class PasswordChange
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ComfirmPassword { get; set; }
    }
}
