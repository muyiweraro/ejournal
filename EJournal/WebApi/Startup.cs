using Blazor.FlexGrid;
using Blazored.LocalStorage;
using Blazored.Modal;
using Blazored.SessionStorage;
using Blazored.Toast;
using Coravel;
using CurrieTechnologies.Razor.SweetAlert2;
using EJournal.Application;
using EJournal.Application.Common.Interfaces;
using EJournal.Application.Common.Models;
using EJournal.Infrastructure;
using EJournal.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.JSInterop;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.Net.Http;
using System.Text;
using WebApi.Common;
using WebApi.Dto;
using WebApi.GridConfig;
using WebApi.HttpServices;
using WebApi.Services;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)

                .CreateLogger();
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(o => {
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))

                    };
                });
            var emailConfig = Configuration
        .GetSection("EmailConfiguration")
        .Get<EmailConfiguration>();
            var domanConfig = Configuration
       .GetSection("Domains")
       .Get<Domains>();
            services.AddSingleton(emailConfig);
            services.AddSingleton(domanConfig);
            services.AddControllers();
            services.AddApplication();
            services.AddInfrastructure(Configuration);
            services.AddAuthorization();
            services.AddAuthentication();
            services.AddBlazoredSessionStorage();
            services.AddBlazoredModal();
            services.AddBlazoredLocalStorage();
            services.AddCache();
            //services.AddMailer(Configuration);


            //services.AddScoped<ITMSNotificationBridgeContext>(provider => provider.GetService<TMSNotificationBridgeContext>());
            services.AddScoped<ICurrentUserService, CurrentUserService>();


            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IClientUserService, ClientUserService>();
            services.AddScoped<IUserManagerService, UserManagerService>();
            services.AddScoped<IEmailSenderService, EmailSenderService>();
           // services.AddSingleton<IJSInProcessRuntime>(services => (IJSInProcessRuntime)services.GetRequiredService<IJSRuntime>());
            services.AddHttpContextAccessor();
            services.AddScoped<AppState>();
            services.AddScoped<HttpContextAccessor>();
            //services.AddScoped<JSInProcessRuntime>();
            //services.TryAddSingleton<IJSInProcessRuntime, JSInProcessRuntime>();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped(s =>
            {
                // Creating the URI helper needs to wait until the JS Runtime is initialized, so defer it.
                var uriHelper = s.GetRequiredService<NavigationManager>();
                return new HttpClient
                {
                    BaseAddress = new Uri(uriHelper.BaseUri)
                };
            });
            //services.AddHttpClient("twitterClient", (client, s) =>
            //{
            //    var uriHelper = client.GetRequiredService<NavigationManager>();
            //    s.BaseAddress = new Uri("https://api.twitter.com");
            //});

            services.AddRazorPages();
            services.AddServerSideBlazor().AddCircuitOptions(options => { options.DetailedErrors = true; });
            services.AddHealthChecks();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "E-Journal API", Version = "v1" });
            });
            services.AddBlazoredToast();
            services.AddSweetAlert2();
            //services.AddMvc()
            //.AddSessionStateTempDataProvider();
            services.AddSession(options =>
            {
                options.Cookie.IsEssential = true;
            });
            services.AddFlexGridServerSide(cfg =>
            {
                cfg.ApplyConfiguration(new ClientDtoGridConfiguration());
                cfg.ApplyConfiguration(new NotificationGridConfig());
                cfg.ApplyConfiguration(new UserGridConfig());
                cfg.ApplyConfiguration(new RoleGridConfig());
            });
            var settings = new Settings();
            Configuration.GetSection("Settings").Bind(settings);
            services.AddSingleton(settings);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseFlexGrid(env.WebRootPath);
            app.UseCustomExceptionHandler();
            app.UseHealthChecks("/health");
            app.UseSwagger();
            app.UseSession();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "E-Journal API V1");
                c.RoutePrefix = string.Empty;
            });
            loggerFactory.AddSerilog();


            // BLAZOR COOKIE Auth Code (begin)


            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseRouting();
            app.UseAuthentication();
            //app.UseIdentityServer();
            app.UseAuthorization();
            //app.UseMvcWithDefaultRoute();
            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapControllers();
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
