﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EJournal.Domain.Entities
{
    public  class Notification
    {
        public long Id { get; set; }
        public string MerchantId { get; set; }
        public string Mti { get; set; }
        public string Processingcode { get; set; }
        public string Amount { get; set; }
        public string Stan { get; set; }
        public string Pan { get; set; }
        public string Track2 { get; set; }
        public string Track1 { get; set; }
        public string Iccdata { get; set; }
        public string Posentrymode { get; set; }
        public string Refcode { get; set; }
        public string Posconditioncode { get; set; }
        public string Aurhorisationresponse { get; set; }
        public string Currencycode { get; set; }
        public string Terminalid { get; set; }
        public string Transactiondate { get; set; }
        public string Transactiontime { get; set; }
        public string Systemtime { get; set; }
        public string Responcecode { get; set; }
        public string Trantype { get; set; }
        public string Batchno { get; set; }
        public string Seqno { get; set; }
        public string TStatus { get; set; }
        public string Responsedescription { get; set; }
        public string RemoteServerIp { get; set; }
        public string FirmwareId { get; set; }
        public string Timelocaltransaction { get; set; }
        public string Datelocaltransaction { get; set; }
        public string Rrn { get; set; }
        public string Trancode { get; set; }
        public string Searchtext { get; set; }
        public string Accounttype { get; set; }
        public string Cellinfo { get; set; }
        public string Gpsinfo { get; set; }
        public string Otherterminalid { get; set; }
        public string ChannelCode { get; set; }
        public string PaymentMethod { get; set; }
        public string CustomerName { get; set; }
        public string RevenueCode { get; set; }
        public string Narration { get; set; }
        public string CustomerOtherInfo { get; set; }
        [NotMapped]
        public string Aid { get; set; }
        [NotMapped]
        public string Card { get; set; }
        [NotMapped]
        public string Tsi { get; set; }
        [NotMapped]
        public string Ac { get; set; }
        [NotMapped]
        public string Tvr { get; set; }
        public bool? TmsPushed { get; set; }
        public bool? ReportPushed { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? TmsPushedDate { get; set; }
        public DateTime? ReportPushedDate { get; set; }
        public bool? PaxTransaction { get; set; }
        public string MerchantName { get; set; }
        public string MerchantAddress { get; set; }
        public string Footer { get; set; }
    }
}
