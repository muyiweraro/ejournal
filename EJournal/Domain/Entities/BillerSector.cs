﻿using System;
using System.Collections.Generic;

namespace EJournal.Domain.Entities
{
    public partial class BillerSector
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
