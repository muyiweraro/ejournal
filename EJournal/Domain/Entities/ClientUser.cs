﻿using EJournal.Domain.Common;
using EJournal.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Domain.Entities
{
   public class ClientUser : AuditableEntity
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string BankCode { get; set; }
        public string MerchantIds { get; set; }
        public ClientType ClientType { get; set; }
        public string UserId { get; set; }
        public string UserSecret { get; set; }
        public string UrlPath { get; set; }
        //[Required]
       // [Range(typeof(bool), "true", "true",
        //ErrorMessage = "This form disallows unapproved client.")]
        public bool Active { get; set; }
        public bool IsDelete { get; set; }
    }
}
