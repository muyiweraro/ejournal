﻿using System;
using System.Collections.Generic;

namespace EJournal.Domain.Entities
{
    public partial class TransactionRow
    {
        public long Id { get; set; }
        public long LastRowNo { get; set; }
        public long CurrentRowNo { get; set; }
        public DateTime LastUpdatedTime { get; set; }
    }
}
