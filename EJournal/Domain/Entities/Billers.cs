﻿using System;
using System.Collections.Generic;

namespace EJournal.Domain.Entities
{
    public partial class Billers
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string PublicId { get; set; }
        public string Ranking { get; set; }
        public string Sector { get; set; }
    }
}
