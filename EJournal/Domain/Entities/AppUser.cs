﻿using EJournal.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace EJournal.Domain.Entities
{
   
    public class AppUser : AuditableEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int ClientId { get; set; }
        public int RoleId { get; set; }
        public string ResetToken { get; set; }
        public virtual Role Role { get; set; }
        public virtual ClientUser Client { get; set; }
    }
}
