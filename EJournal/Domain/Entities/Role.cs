﻿using EJournal.Domain.Common;

namespace EJournal.Domain.Entities
{
    public class Role : AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

   
}
