﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EJournal.Infrastructure.Migrations
{
    public partial class AddResetTokenTAppuser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ResetToken",
                table: "AppUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ResetToken",
                table: "AppUsers");
        }
    }
}
