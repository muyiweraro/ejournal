﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EJournal.Infrastructure.Migrations
{
    public partial class identityClaim : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "billers",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(unicode: false, nullable: false),
                    code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    publicId = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    ranking = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    sector = table.Column<string>(unicode: false, maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "billerSector",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    code = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "ClientUser",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    ClientName = table.Column<string>(nullable: true),
                    ClientEmail = table.Column<string>(nullable: true),
                    BankCode = table.Column<string>(nullable: true),
                    MerchantIds = table.Column<string>(nullable: true),
                    ClientType = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    UserSecret = table.Column<string>(nullable: true),
                    UrlPath = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeviceCodes",
                columns: table => new
                {
                    UserCode = table.Column<string>(maxLength: 200, nullable: false),
                    DeviceCode = table.Column<string>(maxLength: 200, nullable: false),
                    SubjectId = table.Column<string>(maxLength: 200, nullable: true),
                    ClientId = table.Column<string>(maxLength: 200, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    Expiration = table.Column<DateTime>(nullable: false),
                    Data = table.Column<string>(maxLength: 50000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceCodes", x => x.UserCode);
                });

            migrationBuilder.CreateTable(
                name: "Notification",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    merchantId = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    mti = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    processingcode = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    amount = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    stan = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    pan = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    track2 = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    track1 = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    iccdata = table.Column<string>(unicode: false, maxLength: 512, nullable: true),
                    posentrymode = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    refcode = table.Column<string>(unicode: false, nullable: true),
                    posconditioncode = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    aurhorisationresponse = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    currencycode = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    terminalid = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    transactiondate = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    transactiontime = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    systemtime = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    responcecode = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    trantype = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    batchno = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    seqno = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    t_status = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    responsedescription = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    remote_server_ip = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    firmware_id = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    timelocaltransaction = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    datelocaltransaction = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    rrn = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    trancode = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    searchtext = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    accounttype = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    cellinfo = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    gpsinfo = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    otherterminalid = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    channelCode = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    paymentMethod = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    customerName = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    revenueCode = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    narration = table.Column<string>(unicode: false, nullable: true),
                    customerOtherInfo = table.Column<string>(unicode: false, nullable: true),
                    tmsPushed = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    reportPushed = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    createdDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    tmsPushedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    reportPushedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    paxTransaction = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    merchantName = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    merchantAddress = table.Column<string>(unicode: false, nullable: true),
                    footer = table.Column<string>(unicode: false, maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "PAXRow",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    lastRowNo = table.Column<long>(nullable: false),
                    currentRowNo = table.Column<long>(nullable: false),
                    lastUpdatedTime = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "PersistedGrants",
                columns: table => new
                {
                    Key = table.Column<string>(maxLength: 200, nullable: false),
                    Type = table.Column<string>(maxLength: 50, nullable: false),
                    SubjectId = table.Column<string>(maxLength: 200, nullable: true),
                    ClientId = table.Column<string>(maxLength: 200, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    Expiration = table.Column<DateTime>(nullable: true),
                    Data = table.Column<string>(maxLength: 50000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersistedGrants", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TerminalReqResp",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    printer = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    tamper = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    comms = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    keypad = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    lcd = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    readers = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    battery = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    gps = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    power = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    systemtime = table.Column<DateTime>(type: "datetime", nullable: false),
                    terminalid = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    status = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    summary = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    serialno = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    appversion = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    searchtext = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    terminal_id = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    serial_no = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    last_seen = table.Column<DateTime>(type: "datetime", nullable: true),
                    device = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    is_delete = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    imageid = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    modelno = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    modeltype = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    notificationResponse = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "TransactionLogger",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    terminalID = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    stan = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    rrn = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    authCode = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    responseCode = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    amount = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    date = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    pan = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    merchantPINUsed = table.Column<bool>(nullable: true),
                    ReceiptUrl = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    PtspUser = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    notificationResponse = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    createdDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "transactionRow",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    lastRowNo = table.Column<long>(nullable: false),
                    currentRowNo = table.Column<long>(nullable: false),
                    lastUpdatedTime = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    ClientUserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_ClientUser_ClientUserId",
                        column: x => x.ClientUserId,
                        principalTable: "ClientUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceCodes_DeviceCode",
                table: "DeviceCodes",
                column: "DeviceCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DeviceCodes_Expiration",
                table: "DeviceCodes",
                column: "Expiration");

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_Expiration",
                table: "PersistedGrants",
                column: "Expiration");

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_SubjectId_ClientId_Type",
                table: "PersistedGrants",
                columns: new[] { "SubjectId", "ClientId", "Type" });

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Role",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_User_ClientUserId",
                table: "User",
                column: "ClientUserId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "User",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "User",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "billers");

            migrationBuilder.DropTable(
                name: "billerSector");

            migrationBuilder.DropTable(
                name: "DeviceCodes");

            migrationBuilder.DropTable(
                name: "Notification");

            migrationBuilder.DropTable(
                name: "PAXRow");

            migrationBuilder.DropTable(
                name: "PersistedGrants");

            migrationBuilder.DropTable(
                name: "TerminalReqResp");

            migrationBuilder.DropTable(
                name: "TransactionLogger");

            migrationBuilder.DropTable(
                name: "transactionRow");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "ClientUser");
        }
    }
}
