﻿using EJournal.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace EJournal.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ClientId { get; set; }
        public virtual ClientUser ClientUser { get; set; }

    }
}
