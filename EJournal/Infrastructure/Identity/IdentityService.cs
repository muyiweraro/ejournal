﻿using EJournal.Application;
using EJournal.Application.Client;
using EJournal.Application.Client.Queries;
using EJournal.Application.Common.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EJournal.Infrastructure.Identity
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public IMediator Mediator { get; }

        public IdentityService(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager,
            SignInManager<ApplicationUser> signInManager, IMediator Mediator)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            this.Mediator = Mediator;
        }

        public async Task<string> GetUserNameAsync(int userId)
        {
            var user = await _userManager.Users.FirstAsync(u => u.Id == userId);

            return user.UserName;
        }
        public async Task<(Result Result, int UserId)> CreateUserAsync(AppUser appUser)
        {
            var user = new ApplicationUser
            {
                UserName = appUser.Username,
                Email = appUser.Username,
                FirstName = appUser.FirstName,
                LastName = appUser.LastName,
                ClientId = appUser.ClientId
            };

            var result = await _userManager.CreateAsync(user, appUser.Password);
            if (result.Succeeded)
            {
                var findUserById = await _userManager.FindByNameAsync(user.UserName);
                if(findUserById != null)
                {
                    await _userManager.AddToRoleAsync(findUserById, appUser.RoleName);
                }
                 
            }

            return (result.ToApplicationResult(), user.Id);
        }

        //public async Task<AppUser> Login(string username, string password)
        //{
        //    var appUser = await _userManager.FindByEmailAsync(username);
        //    if (appUser != null)
        //    {
        //     var result =  await _userManager.CheckPasswordAsync(appUser,password);
        //        if (result)
        //            return Redirect(login.ReturnUrl ?? "/");
        //    }
        //    //ModelState.AddModelError(nameof(login.Email), "Login Failed: Invalid Email or password");
        //}
        public async Task<Result> DeleteUserAsync(int userId)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

            if (user != null)
            {
                return await DeleteUserAsync(user);
            }

            return Result.Success();
        }

        public async Task<Result> DeleteUserAsync(ApplicationUser user)
        {
            var result = await _userManager.DeleteAsync(user);

            return result.ToApplicationResult();
        }

        public async Task<List<string>> GetAllRoleAsync()
        {
            try
            {
                var roles1 = await _roleManager.Roles.ToListAsync();
                var roles = await _roleManager.Roles.Select(r => r.Name).ToListAsync();
                return roles;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }

        public async Task<(AppUser user, ClientDto client)> LoginAsync(string username, string password)
        {
            var appUser = new AppUser();
            var appClient = new ClientDto();
            try
            {
                var user = await _userManager.FindByNameAsync(username);
                if (user != null)
                {
                    //await _signInManager.SignOutAsync();
                    var result = await _userManager.CheckPasswordAsync(user, password);
                    if (result)
                    {
                        appUser.ClientId = user.ClientId;
                        appUser.FirstName = user.FirstName;
                        appUser.LastName = user.LastName;
                        appUser.Username = user.UserName;
                        appClient = await  Mediator.Send(new GetClientByIdQuery { Id = user.ClientId });
                    }
                   // await _signInManager.SignInAsync
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return (appUser, appClient);
        }

        public async Task<(AppUser user, Client client)> GetUserInfo(string username)
        {
            var appUser = new AppUser();
            var appClient = new Client();
            var user = await _userManager.FindByNameAsync(username);
            if (user != null)
            {
                appUser.ClientId = user.ClientId;
                appUser.FirstName = user.FirstName;
                appUser.LastName = user.LastName;
                appUser.Username = user.UserName;
            }
            throw new NotImplementedException();
        }

        //public Task<(AppUser user, Client client)> GetUserInfo()
        //{
        //    var appUser = new AppUser();
        //    var appClient = new Client();
        //    throw
        //    //if (User?.Identity?.IsAuthenticated )
        //    //{
        //    //    await _signInManager.SignOutAsync();
        //    //    var result = await _signInManager.PasswordSignInAsync(user, password, false, false);
        //    //    if (result.Succeeded)
        //    //        return result.Succeeded;
        //    //    //var result = await _userManager.CheckPasswordAsync(user, password);
        //    //    //if (result)
        //    //    //{
        //    //    //    appUser.ClientId = user.ClientId;
        //    //    //    appUser.FirstName = user.FirstName;
        //    //    //    appUser.LastName = user.LastName;
        //    //    //    appUser.Username = user.UserName;
        //    //    //}
        //    //    return result.IsNotAllowed;
        //    //}

        //    //return false;
        //}
    }
}
