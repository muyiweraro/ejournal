﻿using EJournal.Application.Common.Interfaces;
using EJournal.Domain.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EJournal.Infrastructure.Persistence
{
    public class CentralCollectionReportContext : DbContext, ICentralCollectionReportContext
    {
        public CentralCollectionReportContext(DbContextOptions<CentralCollectionReportContext> options) : base(options)
        {
        }
        public DbSet<Domain.Entities.Institutions> Institutions { get; set; } 
       
    }
}
