﻿using EJournal.Application;
using EJournal.Application.Common.Interfaces;
using EJournal.Infrastructure.Files;
using EJournal.Infrastructure.Identity;
using EJournal.Infrastructure.Persistence;
using EJournal.Infrastructure.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EJournal.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TMSNotificationBridgeContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"), 
                    b => b.MigrationsAssembly(typeof(TMSNotificationBridgeContext).Assembly.FullName)));

            services.AddScoped<ITMSNotificationBridgeContext>(provider => provider.GetService<TMSNotificationBridgeContext>());


            services.AddDbContext<CentralCollectionReportContext>(options =>
               options.UseSqlServer(
                   configuration.GetConnectionString("CentralReportConnection"),
                   b => b.MigrationsAssembly(typeof(CentralCollectionReportContext).Assembly.FullName)));

            services.AddScoped<ICentralCollectionReportContext>(provider => provider.GetService<CentralCollectionReportContext>());

            //services.AddDefaultIdentity<ApplicationUser>()
            // .AddRoles<ApplicationRole>()
            //.AddEntityFrameworkStores<TMSNotificationBridgeContext>()
            //.AddDefaultTokenProviders();

            //services.AddIdentityServer()
            //.AddApiAuthorization<ApplicationUser, TMSNotificationBridgeContext>();
            services.AddTransient<IDateTime, DateTimeService>();
           // services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<ICsvFileBuilder, CsvFileBuilder>();
            //services.AddDefaultIdentity<ApplicationUser>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>();

            //if (environment.IsEnvironment("Test"))
            //{
            //    services.AddIdentityServer()
            //        .AddApiAuthorization<ApplicationUser, ApplicationDbContext>(options =>
            //        {
            //            options.Clients.Add(new Client
            //            {
            //                ClientId = "EJournal.IntegrationTests",
            //                AllowedGrantTypes = { GrantType.ResourceOwnerPassword },
            //                ClientSecrets = { new Secret("secret".Sha256()) },
            //                AllowedScopes = { "EJournal.WebUIAPI", "openid", "profile" }
            //            });
            //        }).AddTestUsers(new List<TestUser>
            //        {
            //            new TestUser
            //            {
            //                SubjectId = "f26da293-02fb-4c90-be75-e4aa51e0bb17",
            //                Username = "jason@clean-architecture",
            //                Password = "EJournal!",
            //                Claims = new List<Claim>
            //                {
            //                    new Claim(JwtClaimTypes.Email, "jason@clean-architecture")
            //                }
            //            }
            //        });
            //}
            //else
            //{
            //    services.AddIdentityServer()
            //        .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            //    services.AddTransient<IDateTime, DateTimeService>();
            //    services.AddTransient<IIdentityService, IdentityService>();
            //    services.AddTransient<ICsvFileBuilder, CsvFileBuilder>();
            //}

            //services.AddAuthentication()
            //    .AddIdentityServerJwt();

            return services;
        }
    }
}
