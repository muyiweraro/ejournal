﻿using CsvHelper.Configuration;

namespace EJournal.Infrastructure.Files.Maps
{
    public class ObjectRecordMap : ClassMap<object>
    {
        public ObjectRecordMap()
        {
            AutoMap();
            //Map(m => m.Done).ConvertUsing(c => c.Done ? "Yes" : "No");
        }
    }
}
