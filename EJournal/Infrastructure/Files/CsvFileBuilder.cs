﻿using EJournal.Application.Common.Interfaces;
using EJournal.Infrastructure.Files.Maps;
using CsvHelper;
using System.Collections.Generic;
using System.IO;
using EJournal.Domain.Entities;

namespace EJournal.Infrastructure.Files
{
    public class CsvFileBuilder : ICsvFileBuilder
    {
        public byte[] BuildTodoItemsFile(IEnumerable<object> records)
        {
            using var memoryStream = new MemoryStream();
            using (var streamWriter = new StreamWriter(memoryStream))
            {
                using var csvWriter = new CsvWriter(streamWriter);

                csvWriter.Configuration.RegisterClassMap<ObjectRecordMap>();
                csvWriter.WriteRecords(records);
            }

            return memoryStream.ToArray();
        }
    }
}
