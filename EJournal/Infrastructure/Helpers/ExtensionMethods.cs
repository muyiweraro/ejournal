﻿using EJournal.Application.Common.Models;
using EJournal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EJournal.Infrastructure.Helpers
{
    public static class ExtensionMethods
    {
        public static IEnumerable<ClientUser> WithoutPasswords(this IEnumerable<ClientUser> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static ClientUser WithoutPassword(this ClientUser user)
        {
            user.UserSecret = null;
            return user;
        }

        public static IQueryable<TSource> WhereIf<TSource>(
        this IQueryable<TSource> source,
        bool condition,
        Func<TSource, bool> predicate)
        {
            if (condition)
                return source.Where(predicate).AsQueryable();
            else
                return source;
        }
    }
}
