﻿using System;
using System.Collections.Generic;

namespace EJournal.Infrastructure.Models
{
    public partial class Paxrow
    {
        public long Id { get; set; }
        public long LastRowNo { get; set; }
        public long CurrentRowNo { get; set; }
        public DateTime LastUpdatedTime { get; set; }
    }
}
