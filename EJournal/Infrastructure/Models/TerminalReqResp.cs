﻿using System;
using System.Collections.Generic;

namespace EJournal.Infrastructure.Models
{
    public partial class TerminalReqResp
    {
        public long Id { get; set; }
        public string Printer { get; set; }
        public string Tamper { get; set; }
        public string Comms { get; set; }
        public string Keypad { get; set; }
        public string Lcd { get; set; }
        public string Readers { get; set; }
        public string Battery { get; set; }
        public string Gps { get; set; }
        public string Power { get; set; }
        public DateTime Systemtime { get; set; }
        public string Terminalid { get; set; }
        public string Status { get; set; }
        public string Summary { get; set; }
        public string Serialno { get; set; }
        public string Appversion { get; set; }
        public string Searchtext { get; set; }
        public string TerminalId1 { get; set; }
        public string SerialNo1 { get; set; }
        public DateTime? LastSeen { get; set; }
        public string Device { get; set; }
        public string Name { get; set; }
        public string IsDelete { get; set; }
        public string Imageid { get; set; }
        public string Modelno { get; set; }
        public string Modeltype { get; set; }
        public string NotificationResponse { get; set; }
    }
}
