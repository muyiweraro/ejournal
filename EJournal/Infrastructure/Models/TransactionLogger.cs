﻿using System;
using System.Collections.Generic;

namespace EJournal.Infrastructure.Models
{
    public partial class TransactionLogger
    {
        public long Id { get; set; }
        public string TerminalId { get; set; }
        public string Stan { get; set; }
        public string Rrn { get; set; }
        public string AuthCode { get; set; }
        public string ResponseCode { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Pan { get; set; }
        public bool? MerchantPinused { get; set; }
        public string ReceiptUrl { get; set; }
        public string PtspUser { get; set; }
        public string NotificationResponse { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
