﻿using System;
using System.Collections.Generic;

namespace EJournal.Infrastructure.Models
{
    public partial class ClientUser
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string BankCode { get; set; }
        public string MerchantIds { get; set; }
        public int ClientType { get; set; }
        public string UserId { get; set; }
        public string UserSecret { get; set; }
        public string UrlPath { get; set; }
        public bool Active { get; set; }
        public bool IsDelete { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastModified { get; set; }
        public string LastModifiedBy { get; set; }
    }
}
