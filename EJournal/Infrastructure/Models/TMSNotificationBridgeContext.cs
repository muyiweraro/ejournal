﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EJournal.Infrastructure.Models
{
    public partial class TMSNotificationBridgeContext : DbContext
    {
        public TMSNotificationBridgeContext()
        {
        }

        public TMSNotificationBridgeContext(DbContextOptions<TMSNotificationBridgeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BillerSector> BillerSector { get; set; }
        public virtual DbSet<Billers> Billers { get; set; }
        public virtual DbSet<ClientUser> ClientUser { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<Paxrow> Paxrow { get; set; }
        public virtual DbSet<TerminalReqResp> TerminalReqResp { get; set; }
        public virtual DbSet<TransactionLogger> TransactionLogger { get; set; }
        public virtual DbSet<TransactionRow> TransactionRow { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data source=172.22.55.25,1514; Initial Catalog=TMSNotificationBridge; user=sa; password=SqlDev@1");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BillerSector>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("billerSector");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Billers>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("billers");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .IsUnicode(false);

                entity.Property(e => e.PublicId)
                    .IsRequired()
                    .HasColumnName("publicId")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Ranking)
                    .HasColumnName("ranking")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Sector)
                    .HasColumnName("sector")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientUser>(entity =>
            {
                entity.Property(e => e.UserId).IsRequired();
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Accounttype)
                    .HasColumnName("accounttype")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Amount)
                    .IsRequired()
                    .HasColumnName("amount")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Aurhorisationresponse)
                    .HasColumnName("aurhorisationresponse")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Batchno)
                    .IsRequired()
                    .HasColumnName("batchno")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Cellinfo)
                    .HasColumnName("cellinfo")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ChannelCode)
                    .HasColumnName("channelCode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Currencycode)
                    .IsRequired()
                    .HasColumnName("currencycode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasColumnName("customerName")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerOtherInfo)
                    .HasColumnName("customerOtherInfo")
                    .IsUnicode(false);

                entity.Property(e => e.Datelocaltransaction)
                    .IsRequired()
                    .HasColumnName("datelocaltransaction")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FirmwareId)
                    .HasColumnName("firmware_id")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Footer)
                    .HasColumnName("footer")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Gpsinfo)
                    .HasColumnName("gpsinfo")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Iccdata)
                    .HasColumnName("iccdata")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.MerchantAddress)
                    .HasColumnName("merchantAddress")
                    .IsUnicode(false);

                entity.Property(e => e.MerchantId)
                    .HasColumnName("merchantId")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.MerchantName)
                    .HasColumnName("merchantName")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Mti)
                    .IsRequired()
                    .HasColumnName("mti")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Narration)
                    .HasColumnName("narration")
                    .IsUnicode(false);

                entity.Property(e => e.Otherterminalid)
                    .HasColumnName("otherterminalid")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Pan)
                    .IsRequired()
                    .HasColumnName("pan")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.PaxTransaction)
                    .HasColumnName("paxTransaction")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PaymentMethod)
                    .HasColumnName("paymentMethod")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Posconditioncode)
                    .IsRequired()
                    .HasColumnName("posconditioncode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Posentrymode)
                    .IsRequired()
                    .HasColumnName("posentrymode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Processingcode)
                    .HasColumnName("processingcode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Refcode)
                    .HasColumnName("refcode")
                    .IsUnicode(false);

                entity.Property(e => e.RemoteServerIp)
                    .HasColumnName("remote_server_ip")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ReportPushed)
                    .HasColumnName("reportPushed")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ReportPushedDate)
                    .HasColumnName("reportPushedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Responcecode)
                    .IsRequired()
                    .HasColumnName("responcecode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Responsedescription)
                    .HasColumnName("responsedescription")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RevenueCode)
                    .HasColumnName("revenueCode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Rrn)
                    .IsRequired()
                    .HasColumnName("rrn")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Searchtext)
                    .HasColumnName("searchtext")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Seqno)
                    .IsRequired()
                    .HasColumnName("seqno")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Stan)
                    .IsRequired()
                    .HasColumnName("stan")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Systemtime)
                    .HasColumnName("systemtime")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TStatus)
                    .IsRequired()
                    .HasColumnName("t_status")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Terminalid)
                    .IsRequired()
                    .HasColumnName("terminalid")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Timelocaltransaction)
                    .IsRequired()
                    .HasColumnName("timelocaltransaction")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TmsPushed)
                    .HasColumnName("tmsPushed")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TmsPushedDate)
                    .HasColumnName("tmsPushedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Track1)
                    .HasColumnName("track1")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Track2)
                    .IsRequired()
                    .HasColumnName("track2")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Trancode)
                    .HasColumnName("trancode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Transactiondate)
                    .IsRequired()
                    .HasColumnName("transactiondate")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Transactiontime)
                    .IsRequired()
                    .HasColumnName("transactiontime")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Trantype)
                    .HasColumnName("trantype")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Paxrow>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PAXRow");

                entity.Property(e => e.CurrentRowNo).HasColumnName("currentRowNo");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.LastRowNo).HasColumnName("lastRowNo");

                entity.Property(e => e.LastUpdatedTime)
                    .HasColumnName("lastUpdatedTime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<TerminalReqResp>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Appversion)
                    .HasColumnName("appversion")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Battery)
                    .HasColumnName("battery")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Comms)
                    .IsRequired()
                    .HasColumnName("comms")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Device)
                    .HasColumnName("device")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Gps)
                    .HasColumnName("gps")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Imageid)
                    .HasColumnName("imageid")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete)
                    .IsRequired()
                    .HasColumnName("is_delete")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Keypad)
                    .IsRequired()
                    .HasColumnName("keypad")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LastSeen)
                    .HasColumnName("last_seen")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lcd)
                    .IsRequired()
                    .HasColumnName("lcd")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Modelno)
                    .HasColumnName("modelno")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Modeltype)
                    .HasColumnName("modeltype")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationResponse)
                    .HasColumnName("notificationResponse")
                    .IsUnicode(false);

                entity.Property(e => e.Power)
                    .IsRequired()
                    .HasColumnName("power")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Printer)
                    .IsRequired()
                    .HasColumnName("printer")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Readers)
                    .HasColumnName("readers")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Searchtext)
                    .HasColumnName("searchtext")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNo1)
                    .HasColumnName("serial_no")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Serialno)
                    .HasColumnName("serialno")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Summary)
                    .HasColumnName("summary")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Systemtime)
                    .HasColumnName("systemtime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Tamper)
                    .IsRequired()
                    .HasColumnName("tamper")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TerminalId1)
                    .HasColumnName("terminal_id")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Terminalid)
                    .IsRequired()
                    .HasColumnName("terminalid")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TransactionLogger>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.AuthCode)
                    .HasColumnName("authCode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.MerchantPinused).HasColumnName("merchantPINUsed");

                entity.Property(e => e.NotificationResponse)
                    .HasColumnName("notificationResponse")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Pan)
                    .HasColumnName("pan")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.PtspUser)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiptUrl)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ResponseCode)
                    .HasColumnName("responseCode")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Rrn)
                    .HasColumnName("rrn")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Stan)
                    .HasColumnName("stan")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TerminalId)
                    .HasColumnName("terminalID")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TransactionRow>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("transactionRow");

                entity.Property(e => e.CurrentRowNo).HasColumnName("currentRowNo");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.LastRowNo).HasColumnName("lastRowNo");

                entity.Property(e => e.LastUpdatedTime)
                    .HasColumnName("lastUpdatedTime")
                    .HasColumnType("datetime");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
