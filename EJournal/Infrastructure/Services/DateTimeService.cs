﻿using EJournal.Application.Common.Interfaces;
using System;

namespace EJournal.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
