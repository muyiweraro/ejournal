﻿using EJournal.Application.Common.Interfaces;
using EJournal.Application.Common.Models;
using EJournal.Domain.Entities;
using EJournal.Infrastructure.Helpers;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EJournal.Infrastructure.Services
{
    public interface IClientService
    {
        Task<ClientUser> Authenticate(string username, string password);
        Task<IEnumerable<ClientUser>> GetAll();
        Task<bool> IsValidUser(string username, string password);
    }

    public class ClientService : IClientService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private readonly ITMSNotificationBridgeContext _context;
        private IMemoryCache _cache;

        public ClientService(ITMSNotificationBridgeContext context, IMemoryCache memoryCache)
        {
            _context = context;
            _cache = memoryCache;
        }
        public async Task<ClientUser> Authenticate(string username, string password)
        {
            //var user = await Task.Run(() => _users.SingleOrDefault(x => x.ClientId == username && x.ClientSerect == password));
            var client = await Task.Run(() => _context.ClientUser.SingleOrDefault(x => x.UserId == username && x.UserSecret == password));

            // return null if user not found
            if (client == null)
                return null;

            // authentication successful so return user details without password

            return client.WithoutPassword();
        }

        public async Task<IEnumerable<ClientUser>> GetAll()
        {
            return await Task.Run(() => _context.ClientUser.WithoutPasswords());
        }

        public async Task<bool> IsValidUser(string username, string password)
        {
            _cache.Remove(username);
            var user = await Task.Run(() => _context.ClientUser.SingleOrDefault(x => x.UserId == username && x.UserSecret == password));

            // return null if user not found
            if (user == null)
                return false;
            _cache.Set(username, user);
            return true;
        }
    }
}
