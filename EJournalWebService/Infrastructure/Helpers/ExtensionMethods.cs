﻿using EJournalWebService.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EJournalWebService.Infrastructure.Helpers
{
    public static class ExtensionMethods
    {
        public static IEnumerable<Client> WithoutPasswords(this IEnumerable<Client> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static Client WithoutPassword(this Client user)
        {
            user.ClientSerect = null;
            return user;
        }
    }
}
