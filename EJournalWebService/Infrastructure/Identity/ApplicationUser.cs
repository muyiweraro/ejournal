﻿using Microsoft.AspNetCore.Identity;

namespace EJournalWebService.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
