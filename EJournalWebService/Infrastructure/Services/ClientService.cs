﻿using EJournalWebService.Application.Common.Models;
using EJournalWebService.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EJournalWebService.Infrastructure.Services
{
    public interface IClientService
    {
        Task<Client> Authenticate(string username, string password);
        Task<IEnumerable<Client>> GetAll();
        Task<bool> IsValidUser(string username, string password);
    }

    public class ClientService : IClientService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private List<Client> _users = new List<Client>
        {
            new Client { Id = 1, ClientId = "Test", ClientName = "User", ClientSerect = "test", MerchantIds = "test" }
        };

        public async Task<Client> Authenticate(string username, string password)
        {
            var user = await Task.Run(() => _users.SingleOrDefault(x => x.ClientId == username && x.ClientSerect == password));

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so return user details without password
            return user.WithoutPassword();
        }

        public async Task<IEnumerable<Client>> GetAll()
        {
            return await Task.Run(() => _users.WithoutPasswords());
        }

        public async Task<bool> IsValidUser(string username, string password)
        {
            var user = await Task.Run(() => _users.SingleOrDefault(x => x.ClientId == username && x.ClientSerect == password));

            // return null if user not found
            if (user == null)
                return false;

            // authentication successful so return user details without password
            return true;
        }
    }
}
