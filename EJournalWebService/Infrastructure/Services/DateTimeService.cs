﻿using EJournalWebService.Application.Common.Interfaces;
using System;

namespace EJournalWebService.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
