﻿using EJournalWebService.Application.Common.Mappings;
using EJournalWebService.Domain.Entities;

namespace EJournalWebService.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
