﻿using EJournalWebService.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace EJournalWebService.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
